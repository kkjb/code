package com.hmdp.service;

//基于redis实现分布式锁（我们采用的是互斥的非阻塞式的分布式锁，它确保只能有一个线程获取锁，它只会获取锁一次，当获取锁失败的时候，它不会去不断重试，而是成功返回true，失败返回false）
public interface ILock {

	/**
	 * 尝试获取锁
	 *
	 * @Param timeoutSec 锁持有的超时时间，过期后自动释放
	 * @Return true代表获取锁成功;false代表获取锁失败
	 */
	boolean tryLock(long timeoutSec);

	/**
	 * 释放锁
	 */
	void unlock();

}
