package com.hmdp.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hmdp.dto.Result;
import com.hmdp.dto.UserDTO;
import com.hmdp.entity.Follow;
import com.hmdp.mapper.FollowMapper;
import com.hmdp.service.IFollowService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hmdp.service.IUserService;
import com.hmdp.utils.UserHolder;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 虎哥
 * @since 2021-12-22
 */
@Service
public class FollowServiceImpl extends ServiceImpl<FollowMapper, Follow> implements IFollowService {
	@Resource
	private StringRedisTemplate stringRedisTemplate;
	@Resource
	private IUserService userService;

	//关注或取关
	@Override
	public Result follow(Long followUserId, Boolean isFollow) {
		//1.获取登录用户
		Long userId = UserHolder.getUser().getId();
		String key="follows:"+userId;
		//1.判断到底关注还是取关
		if (isFollow) {
			//2.关注，新增数据
			Follow follow = new Follow();
			//添加当前用户（张三关注了李四，添加张三进去）
			follow.setUserId(userId);
			//添加被关注的用户（添加李四进去）
			follow.setFollowUserId(followUserId);
			boolean isSuccess = save(follow);
			//判断添加数据库是否成功
			if(isSuccess){
				//如果添加数据库成功，则把关注用户的id存入redis当中，方便使用共同好友功能 sadd userId followerUserId
				stringRedisTemplate.opsForSet().add(key,followUserId.toString());
			}
		} else {
			//3.取关，删除delete from tb_follow where user_id = ? and follow_user_id = ?，<Follow>代表Follow类
			boolean isSuccess = remove(new QueryWrapper<Follow>().eq("user_id", userId).eq("follow_user_id", followUserId));
			//判断从数据库中删除数据是否成功
			if(isSuccess){
				//把关注的用户从redis中移除
				stringRedisTemplate.opsForSet().remove(key,followUserId.toString());
			}
		}
		return Result.ok();
	}

	//判断用户是否关注
	@Override
	public Result isFollow(Long followUserId) {
		//1.获取登录用户
		Long userId = UserHolder.getUser().getId();
		//2.查询是否关注select from tb_follow where user_id = ? and follow_user_id = ?，<Follow>代表Follow类
		Integer count = query().eq("user_id", userId).eq("follow_user_id", followUserId).count();
		//3.判断（>0表示关注了，=0表示没关注）
		return Result.ok(count>0);
	}

	@Override
	public Result followCommons(Long followUserId) {
		//1.获取登录用户
		Long userId = UserHolder.getUser().getId();
		String key="follows:"+userId;
		//2.被关注的用户
		String key2="follows:"+followUserId;
		//2.求交集
		Set<String> intersect = stringRedisTemplate.opsForSet().intersect(key, key2);
		//防止没有交集，所以我们需要对这个进行判断
		if (intersect==null||intersect.isEmpty()){
			//返回空集合
			return Result.ok(Collections.emptyList());
		}
		//3.解析出id集合
		List<Long> ids = intersect.stream().map(Long::valueOf).collect(Collectors.toList());
		//4.批量查询用户
		List<UserDTO> users = userService.listByIds(ids)
				//5.去除敏感信息，变成userDTO返回
				.stream().map(user -> BeanUtil.copyProperties(user, UserDTO.class))
				.collect(Collectors.toList());
		return Result.ok(users);
	}
}
