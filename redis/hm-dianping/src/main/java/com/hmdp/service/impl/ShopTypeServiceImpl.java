package com.hmdp.service.impl;

import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.hmdp.dto.Result;
import com.hmdp.entity.ShopType;
import com.hmdp.mapper.ShopTypeMapper;
import com.hmdp.service.IShopTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 虎哥
 * @since 2021-12-22
 */
@Service
public class ShopTypeServiceImpl extends ServiceImpl<ShopTypeMapper, ShopType> implements IShopTypeService {

	@Resource
	private StringRedisTemplate stringRedisTemplate;
	@Override
	public Result queryTypeList() {
		String key="cache:typeList";
		//1.从redis中查询店铺类型缓存(range获取指定下标间的值)
		List<String> shopTypeList=stringRedisTemplate.opsForList().range(key, 0, -1);//获取所有值，固定写法
		//2.判断是否存在
		if(!shopTypeList.isEmpty()){
			//3.redis缓存中存在，直接返回
			//创建一个list用来存储返回的list
			List<ShopType> typeList=new ArrayList<>();
			//循环遍历，取出list
			for(String shopTypeJson:shopTypeList){
				//把json格式的数据转成shoptype类型
				ShopType shopType = JSONUtil.toBean(shopTypeJson, ShopType.class);
				//把数据一个一个存入到创建好的list中去
				typeList.add(shopType);
			}
			//返回存好数据的list
			return Result.ok(typeList);
		}
		//4.redis缓存中不存在，查询数据库
		List<ShopType> typeList =query().orderByAsc("sort").list();
		//5.数据库中不存在，返回错误
		if(typeList.isEmpty()){
			return Result.fail("不存在分类");
		}
		//一个一个提取数据库中的数据
		for (ShopType shopType : typeList) {
			//把json的转成string类型
			String shopTypeJson=JSONUtil.toJsonStr(shopType);
			//添加进list中
			shopTypeList.add(shopTypeJson);
		}
		//6.数据库中存在，把上面的list写入redis缓存
		stringRedisTemplate.opsForList().rightPushAll(key,shopTypeList);
		return Result.ok(typeList);
	}

}
