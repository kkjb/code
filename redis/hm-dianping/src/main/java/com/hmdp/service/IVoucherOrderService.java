package com.hmdp.service;

import com.hmdp.dto.Result;
import com.hmdp.entity.VoucherOrder;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 虎哥
 * @since 2021-12-22
 */
public interface IVoucherOrderService extends IService<VoucherOrder> {

	Result seckillVoucher(Long voucherId);

	//IVoucherOrderService proxy = (IVoucherOrderService) AopContext.currentProxy();是直接使用实现类里的，所以我们应该在IVoucherOrderService里创建这个函数
	void createVoucherOrder(VoucherOrder voucherOrder);

}
