package com.hmdp.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import cn.hutool.core.lang.UUID;
import cn.hutool.core.util.RandomUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hmdp.dto.LoginFormDTO;
import com.hmdp.dto.Result;
import com.hmdp.dto.UserDTO;
import com.hmdp.entity.User;
import com.hmdp.mapper.UserMapper;
import com.hmdp.service.IUserService;
import com.hmdp.utils.RegexUtils;
import com.hmdp.utils.UserHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.connection.BitFieldSubCommands;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.hmdp.utils.RedisConstants.*;
import static com.hmdp.utils.SystemConstants.USER_NICK_NAME_PREFIX;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 虎哥
 * @since 2021-12-22
 */
@Slf4j
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {
	@Resource
	private StringRedisTemplate stringRedisTemplate;

	//发送邮箱验证码的自动注入
	// @Autowired
	// private JavaMailSender mailSender;

	//发送短信验证码，并把验证码储存入session
	// @Override
	// public Result sendCode(String phone, HttpSession session) {
	// 	//1.校验手机号
	// 	if (RegexUtils.isPhoneInvalid(phone)) {
	// 		//2.如果不符合，返回错误信息
	// 		return Result.fail("手机号格式错误!");
	// 	}
	// 	//3.符合，生成验证码，RandomUtil类里的随机数生成器
	// 	String code=RandomUtil.randomNumbers(6);
	// 	//4.保存验证码到session
	// 	session.setAttribute("code",code);
	// 	//5.发送验证码（假发送，用日志形式提示发送成功）
	// 	log.debug("发送验证码成功，验证码：{}",code);
	// 	//返回ok
	// 	return Result.ok();
	// }

	//发送短信验证码，并把验证码储存入redis
	@Override
	public Result sendCode(String phone, HttpSession session) {
		//1.校验手机号
		if (RegexUtils.isPhoneInvalid(phone)) {
			//2.如果不符合，返回错误信息
			return Result.fail("手机号格式错误!");
		}
		//3.符合，生成验证码，RandomUtil类里的随机数生成器
		String code=RandomUtil.randomNumbers(6);
		//4.保存验证码到redis //相当于redis中的set key value ex
		stringRedisTemplate.opsForValue().set(LOGIN_CODE_KEY+phone,code,LOGIN_CODE_TTL, TimeUnit.MINUTES);
		//5.发送验证码（假发送，用日志形式提示发送成功）
		log.debug("发送验证码成功，验证码：{}",code);
		//返回ok
		return Result.ok();
	}

	//发送邮箱验证码
	// @Override
	// public Result sendCode(String email, HttpSession session) {
	// 	// 1. 校验邮箱格式
	// 	if (RegexUtils.isEmailInvalid(email)) {
	// 		return Result.fail("邮箱格式错误!");
	// 	}
	// 	// 2. 生成验证码
	// 	String code = RandomUtil.randomNumbers(6);
	// 	// 3. 保存验证码到 Redis
	// 	stringRedisTemplate.opsForValue().set(LOGIN_CODE_KEY + email, code, LOGIN_CODE_TTL, TimeUnit.MINUTES);
	// 	// 4. 发送验证码
	// 	try {
	// 		// 创建邮件消息
	// 		SimpleMailMessage message = new SimpleMailMessage();
	// 		// 设置发送人
	// 		message.setFrom("626372633@qq.com");
	// 		// 设置接收人
	// 		message.setTo(email);
	// 		// 设置主题
	// 		message.setSubject("验证码");
	// 		// 设置正文
	// 		message.setText("您的验证码为：" + code);
	// 		// 发送邮件
	// 		mailSender.send(message);
	// 		// 打印日志
	// 		log.debug("发送验证码成功，验证码：{}", code);
	// 		// 返回成功信息
	// 		return Result.ok();
	// 	} catch (MailException e) {
	// 		// 发送邮件失败
	// 		log.error("发送验证码失败：{}", e.getMessage());
	// 		return Result.fail("发送验证码失败，请稍后重试！");
	// 	}
	// }

	//用session中的验证码登录
	// @Override
	// public Result login(LoginFormDTO loginForm, HttpSession session) {
	// 	//1.校验手机号
	// 	//取出前端传来的手机号
	// 	String phone = loginForm.getPhone();
	// 	if (RegexUtils.isPhoneInvalid(phone)) {
	// 		//2.如果不符合，返回错误信息
	// 		return Result.fail("手机号格式错误!");
	// 	}
	// 	//2.校验验证码(取出session的code和前端传来的code进行对比)
	// 	Object cashCode = session.getAttribute("code");
	// 	String code=loginForm.getCode();
	// 	//反向验证，避免代码越来越深
	// 	if(cashCode==null||!cashCode.toString().equals(code)){
	// 		//3.不一致，报错
	// 		return Result.fail("验证码错误");
	// 	}
	// 	//4.一致，根据手机号查询用户 select * from tb_user where phone = ?
	// 	User user = query().eq("phone", phone).one();
	// 	//5.判断用户是否存在
	// 	if(user == null){
	// 		//6.不存在，创建新用户并保存
	// 		user=createUserWithPhone(phone);
	// 	}
	// 	//7.存在（存在或不存在，都需要将用户信息存入session）(存的东西不能放敏感东西，所以我们应该存UserDTO类型的)
	// 	session.setAttribute("user", BeanUtil.copyProperties(user, UserDTO.class));
	// 	return Result.ok();
	// }

	//用redis中的验证码登录
	@Override
	public Result login(LoginFormDTO loginForm, HttpSession session) {
		//1.校验手机号
		//取出前端传来的手机号
		String phone = loginForm.getPhone();
		if (RegexUtils.isPhoneInvalid(phone)) {
			//2.如果不符合，返回错误信息
			return Result.fail("手机号格式错误!");
		}
		//3.从redis获取验证码并校验
		String cashCode = stringRedisTemplate.opsForValue().get(LOGIN_CODE_KEY+phone);
		String code=loginForm.getCode();
		//反向验证，避免代码越来越深
		if(cashCode==null||!cashCode.equals(code)){
			//不一致，报错
			return Result.fail("验证码错误");
		}
		//4.一致，根据手机号查询用户 select * from tb_user where phone = ?
		User user = query().eq("phone", phone).one();
		//5.判断用户是否存在
		if(user == null){
			//6.不存在，创建新用户并保存
			user=createUserWithPhone(phone);
		}
		//7.保存用户信息到redis中
		//7.1随机生成token，作为登录令牌（true表示带短划线）
		String token = UUID.randomUUID().toString(true);
		//7.2将user对象变成userDTO
		UserDTO userDTO = BeanUtil.copyProperties(user, UserDTO.class);
		/*
		把userDTO转成map存储，
		第一个参数表示需要转换的对象
		第二个参数是新建一个hashmap用于存储，
		第三个参数表示数据拷贝的选项（setIgnoreNullValue(true)忽略新的值，setFieldValueEditor((fieldName,fieldValue)->(fieldValue.toString()))表示对字段值的处理，返回值string是修改后的字段值）
		 */
		Map<String, Object> userMap = BeanUtil.beanToMap(userDTO,new HashMap<>(),
				CopyOptions.create()
						.setIgnoreNullValue(true)
						.setFieldValueEditor((fieldName,fieldValue)->(fieldValue.toString())));
		//7.3存储
		String tokenKey=LOGIN_USER_KEY+token;
		stringRedisTemplate.opsForHash().putAll(tokenKey,userMap);
		//7.4设置token有效期
		// stringRedisTemplate.expire(tokenKey,LOGIN_USER_TTL,TimeUnit.MINUTES);
		//为避免token过期，改长时间
		stringRedisTemplate.expire(tokenKey,114514L,TimeUnit.DAYS);
		//8.返回token
		return Result.ok(token);
	}

	@Override
	public Result sign() {
		//1.获取当前登录的用户
		Long userId = UserHolder.getUser().getId();
		//2.获取当前日期
		LocalDateTime now = LocalDateTime.now();
		//3.拼接key
		//获取当前年份和月份，作为key的后缀
		String keySuffix = now.format(DateTimeFormatter.ofPattern(":yyyyMM"));
		String key=USER_SIGN_KEY+userId+keySuffix;
		//4.今天是本月的第几天，往redis的bitmap存的时候-1就是offset值
		int dayOfMonth = now.getDayOfMonth();
		//5.写入redis，SETBIT bm1（key） 0（offset，代表是bitmap里面的第几位，offset+1就是今天是本月几日） 1（value，代表是true或false）
		//dayOfMonth代表今天是本月的第几天，往redis的bitmap存的时候-1就是offset值
		stringRedisTemplate.opsForValue().setBit(key,dayOfMonth-1,true);
		return Result.ok();
	}

	@Override
	public Result signCount() {
		//1.获取当前登录的用户
		Long userId = UserHolder.getUser().getId();
		//2.获取当前日期
		LocalDateTime now = LocalDateTime.now();
		//3.拼接key
		//获取当前年份和月份，作为key的后缀
		String keySuffix = now.format(DateTimeFormatter.ofPattern(":yyyyMM"));
		String key=USER_SIGN_KEY+userId+keySuffix;
		//4.今天是本月的第几天，往redis的bitmap存的时候-1就是offset值
		int dayOfMonth = now.getDayOfMonth();
		//5.获取本月截止今天为止的所有的签到记录，返回的是一个十进制的数字 BITFIELD sign:5:202308 get u28（u代表无符号，28代表第几个bit，今天是几号，就是几个bit位） 0（代表从第几个开始）
		List<Long> result = stringRedisTemplate.opsForValue().bitField(key, BitFieldSubCommands.create().get(BitFieldSubCommands.BitFieldType.unsigned(dayOfMonth)).valueAt(0));
		if(result==null||result.isEmpty()){
			return Result.ok(0);
		}
		//取出result
		Long num = result.get(0);
		if (num==null||num==0){
			return Result.ok(0);
		}
		//6.循环遍历
		//创建用来统计连续签到天数的计数器
		int count=0;
		while (true){
			//6.1让这个数字与1做与运算，得到数字的最后一个bit位
			//6.2判断这个bit位是否为0
			if ((num&1)==0) {
				//6.3如果为0，说明未签到，结束
				break;
			}else {
				//6.4如果不为0，说明已签到，计数器+1
				count++;
			}
			//6.5把数字右移以为，抛弃最后一个bit位，继续下一个bit位
			num>>>=1;
		}
		return Result.ok(count);
	}

	//创建新用户的方法
	private User createUserWithPhone(String phone) {
		//1.创建用户
		User user = new User();
		user.setPhone(phone);
		user.setNickName(USER_NICK_NAME_PREFIX+RandomUtil.randomString(10));
		//2.保存用户
		save(user);
		return user;
	}

}
