package com.hmdp.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hmdp.dto.Result;
import com.hmdp.dto.ScrollResult;
import com.hmdp.dto.UserDTO;
import com.hmdp.entity.Blog;
import com.hmdp.entity.Follow;
import com.hmdp.entity.User;
import com.hmdp.mapper.BlogMapper;
import com.hmdp.service.IBlogService;
import com.hmdp.service.IFollowService;
import com.hmdp.service.IUserService;
import com.hmdp.utils.SystemConstants;
import com.hmdp.utils.UserHolder;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.hmdp.utils.RedisConstants.BLOG_LIKED_KEY;
import static com.hmdp.utils.RedisConstants.FEED_KEY;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 虎哥
 * @since 2021-12-22
 */
@Service
public class BlogServiceImpl extends ServiceImpl<BlogMapper, Blog> implements IBlogService {

	@Resource
	private IUserService userService;

	@Resource
	private StringRedisTemplate stringRedisTemplate;
	@Resource
	private IFollowService followService;

	//分页查询博客
	@Override
	public Result queryHotBlog(Integer current) {
		// 根据用户查询
		Page<Blog> page = query()
				.orderByDesc("liked")
				.page(new Page<>(current, SystemConstants.MAX_PAGE_SIZE));
		// 获取当前页数据
		List<Blog> records = page.getRecords();
		// 查询用户
		records.forEach(blog -> {
			this.queryBlogUser(blog);
			this.isBlogLiked(blog);
		});
		return Result.ok(records);
	}

	//根据id查询博客详情
	@Override
	public Result queryBlogById(Long id) {
		//1.查询blog
		Blog blog = getById(id);
		if (blog == null) {
			return Result.fail("笔记不存在！");
		}
		//2.查询blog用户
		queryBlogUser(blog);
		//3.查询blog是否被点赞了
		isBlogLiked(blog);
		return Result.ok(blog);
	}

	//查询blog是否被点赞了
	private void isBlogLiked(Blog blog) {
		UserDTO user = UserHolder.getUser();
		if (user==null){
			//用户未登录，无需查询是否点赞
			return;
		}
		//1.获取登录用户
		Long userId = user.getId();
		//2.判断当前用户是否已经点赞，去redis中的set集合中去做判断
		String key = BLOG_LIKED_KEY + blog.getId();
		Double score = stringRedisTemplate.opsForZSet().score(key, userId.toString());
		//如果score!=null证明点过赞了，把blog中的isLike改成ture
		blog.setIsLike(score != null);
	}

	//用户点赞功能，一个人只能给一个博客点一个赞，再次点赞将取消点赞
	@Override
	public Result likeBlog(Long id) {
		//1.获取登录用户
		Long userId = UserHolder.getUser().getId();
		//2.判断当前用户是否已经点赞，去redis中的set集合中去做判断
		String key = BLOG_LIKED_KEY + id;
		//ZSCORE z1 m1（以查询m1分数的方式来判断m1是否存在，返回的是m1的分数）
		Double score = stringRedisTemplate.opsForZSet().score(key, userId.toString());
		//如果分数不存在，证明不存在，该用户没有点过赞
		if (score == null) {
			//3.如果未点赞，可以点赞
			//3.1数据库点赞数+1
			boolean isSuccess = update().setSql("liked=liked+1").eq("id", id).update();
			//3.2保存用户到redis的sortedset集合，下次再来时可以作为是否点赞的依据
			if (isSuccess) {
				//ZADD key score1 member1 [score2 member2](zadd z1 当前时间 m1)
				stringRedisTemplate.opsForZSet().add(key, userId.toString(), System.currentTimeMillis());
			}
		} else {
			//4.如果已点赞，取消点赞
			//4.1数据库点赞数-1
			boolean isSuccess = update().setSql("liked=liked-1").eq("id", id).update();
			//4.2把用户从redis中的set集合中移除
			if (isSuccess) {
				//SREM k1 m1
				stringRedisTemplate.opsForZSet().remove(key, userId.toString());
			}
		}
		return Result.ok();
	}

	@Override
	public Result queryBlogLikes(Long id) {
		String key = BLOG_LIKED_KEY + id;
		//1.查询top5的点赞用户 zrange z1 0 4
		Set<String> top5 = stringRedisTemplate.opsForZSet().range(key, 0, 4);
		//判断点赞用户是否为空
		if (top5==null||top5.isEmpty()){
			//如果没人点赞，则返回空集合
			return Result.ok(Collections.emptyList());
		}
		//2.解析其中的用户id
		List<Long> ids = top5.stream().map(Long::valueOf).collect(Collectors.toList());
		//把id用字符串拼接，方便排序查询
		String idStr = StrUtil.join(",", ids);
		//3.根据用户id查询用户 where id in (5,1) order by field (id,5,1)
		List<UserDTO> userDTOs = userService.query()
				//in (5,1) order by field (id,5,1)
				.in("id",ids).last("order by field(id,"+idStr+")").list()
				.stream()
				.map(user -> BeanUtil.copyProperties(user, UserDTO.class))
				.collect(Collectors.toList());
		//4.返回
		return Result.ok(userDTOs);
	}

	//使用Feed流实现推送博客功能，需要在博主保存博客后，使用推的方式，发送到每个粉丝的收件箱，收件箱使用sorted set来接收推送
	@Override
	public Result saveBlog(Blog blog) {
		//1.获取登录用户
		UserDTO user = UserHolder.getUser();
		blog.setUserId(user.getId());
		//2.保存探店博客，然后把博客发给所有的粉丝
		boolean isSuccess = save(blog);
		//判断博客保存是否成功
		if(!isSuccess){
			//如果保存失败则返回失败
			return Result.fail("新增笔记失败！");
		}
		//3.查询博客作者的粉丝 select * from tb_follow where follow_user_id = ?
		List<Follow> follows = followService.query().eq("follow_user_id", user.getId()).list();
		//4.推送博客id给所有粉丝
		for (Follow follow:follows){
			//4.1获取粉丝id
			Long userId=follow.getUserId();
			//4.2推送，每个粉丝都要有一个收件箱，使用sorted set来接收，key为粉丝id
			String key=FEED_KEY+userId;
			//key为粉丝id，value为博主id，得分为时间戳（查询推送博客的时候，通过时间戳排序，迟推送的在最上面）
			stringRedisTemplate.opsForZSet().add(key,blog.getId().toString(),System.currentTimeMillis());
		}
		//3.返回id
		return Result.ok(blog.getId());
	}
	/**
	 * 滚动分页查询参数：
	 * max：第一次查询时，给当前时间戳，因为对于时间来讲，当前时间就是最大值；如果不是第一次查询，max就是上次查询的min值
	 * min：查询的最小值
	 * offset：第一次查询时，给0；如果不是第一次查询，就给上一次查询的时，与其最小值一样的元素个数，上次查询的最小值元素有几个，这里就填几
	 * count：每页多少条，由前端来指定
	 */
	//查询收件箱里的所有的笔记，然后做一个滚动分页
	@Override
	public Result queryBlogOfFollow(Long max, Integer offset) {
		//1.获取当前用户
		Long userId = UserHolder.getUser().getId();
		//2.查询收件箱（ZREVRANGEBYSCORE z1（查询的key） 6（查询的最大值max） 0（查询的最小值min） withscores limit 2（偏移量offset） 3（查询的数据个数count））
		String key=FEED_KEY+userId;
		/**
		 * 返回值：
		 * "m6",
		 * 6,
		 * "m5",
		 * 5,
		 * "m4"
		 * 5
		 */
		Set<ZSetOperations.TypedTuple<String>> typedTuples = stringRedisTemplate.opsForZSet().reverseRangeByScoreWithScores(key, 0, max, offset, 2);
		//3.非空判断
		if (typedTuples==null||typedTuples.isEmpty()){
			return Result.ok();
		}
		//4.解析数据：推进去的blogId和minTime（上次查询的最小时间戳）,offset
		//ids的大小跟查询的大小保持一致
		List<Long> ids=new ArrayList<>(typedTuples.size());
		//初始化最小时间为0，遍历的过程中不断的去覆盖它，直到最后遍历的就是真实最小值
		long minTime=0;
		//初始化偏移量为1，一旦找到与最小时间戳相同的就+1
		int os=1;
		for(ZSetOperations.TypedTuple<String> tuple:typedTuples){
			//4.1获取id
			ids.add(Long.valueOf(tuple.getValue()));
			//4.2获取分数（时间戳）
			//先来的不断的去覆盖最小值，直到最后一个就是真实的最小值了
			long time = tuple.getScore().longValue();
			if(time==minTime){
				//如果当前取到的时间等于最小时间，那说明时间戳一样，偏移量计数器+1
				os++;
			}else {
				//如果不一样，就说明当前取到的时间不是最小时间，就覆盖掉最小时间，并且重置偏移量计数器
				minTime=time;
				os=1;
			}
		}
		//5.根据id查询blog
		String idStr = StrUtil.join(",", ids);
		List<Blog> blogs = query().in("id", ids).last("order by field(id," + idStr + ")").list();
		//把点赞相关信息一块放进去
		for (Blog blog : blogs) {
			//5.1查询blog用户
			queryBlogUser(blog);
			//5.2查询blog是否被点赞了
			isBlogLiked(blog);
		}
		//6.封装并返回
		ScrollResult r = new ScrollResult();
		r.setList(blogs);
		r.setOffset(os);
		r.setMinTime(minTime);
		return Result.ok(r);
	}

	//查询博客中的用户信息
	private void queryBlogUser(Blog blog) {
		Long userId = blog.getUserId();
		User user = userService.getById(userId);
		blog.setName(user.getNickName());
		blog.setIcon(user.getIcon());
	}

}
