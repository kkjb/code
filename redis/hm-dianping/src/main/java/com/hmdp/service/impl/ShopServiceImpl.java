package com.hmdp.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hmdp.dto.Result;
import com.hmdp.entity.Shop;
import com.hmdp.mapper.ShopMapper;
import com.hmdp.service.IShopService;
import com.hmdp.utils.CacheClient;
import com.hmdp.utils.SystemConstants;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.GeoResult;
import org.springframework.data.geo.GeoResults;
import org.springframework.data.redis.connection.RedisGeoCommands;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.domain.geo.GeoReference;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static com.hmdp.utils.RedisConstants.*;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 虎哥
 * @since 2021-12-22
 */
//JSONUtil.toJsonStr把某些类型的数据转成json类型
//JSONUtil.toBean把json转成bean类型的数据
@Service
public class ShopServiceImpl extends ServiceImpl<ShopMapper, Shop> implements IShopService {

	@Resource
	private StringRedisTemplate stringRedisTemplate;

	@Resource
	private CacheClient cacheClient;

	@Override
	public Result queryById(Long id) {
		//设置空值，解决缓存穿透问题
		//this::getById相当于id2->getById(id2)（通过id去查询数据库）
		// Shop shop =cacheClient.queryWithPassThrough(CACHE_SHOP_KEY,id,Shop.class, this::getById,CACHE_SHOP_TTL, TimeUnit.MINUTES);
		//互斥锁，解决缓存击穿问题
		Shop shop = cacheClient.queryWithMutex(CACHE_SHOP_KEY, id, Shop.class, this::getById, CACHE_SHOP_TTL, TimeUnit.MINUTES);
		//逻辑过期，解决缓存击穿问题
		// Shop shop = cacheClient.queryWithLogicalExpire(CACHE_SHOP_KEY,id, Shop.class,this::getById,CACHE_SHOP_TTL, TimeUnit.MINUTES);
		if (shop == null) {
			return Result.fail("店铺不存在!");
		}
		//返回
		return Result.ok(shop);
	}

	//更新数据库
	@Override
	@Transactional
	public Result update(Shop shop) {
		Long id = shop.getId();
		//判断shop里面id是否为空
		if (id == null) {
			return Result.fail("店铺id不能为空");
		}
		//1.更新数据库
		updateById(shop);
		//2.删除缓存(如果是分布式系统，则需要用mq去通知对方)
		stringRedisTemplate.delete(CACHE_SHOP_KEY + id);
		return Result.ok();
	}

	@Override
	public Result queryShopByType(Integer typeId, Integer current, Double x, Double y) {
		//1.判断是否根据坐标查询（有没有传递x和y进来）
		if (x == null || y == null) {
			//不需要坐标查询，按数据库查
			// 根据类型分页查询
			Page<Shop> page = query()
					.eq("type_id", typeId)
					.page(new Page<>(current, SystemConstants.DEFAULT_PAGE_SIZE));
			// 返回数据
			return Result.ok(page.getRecords());
		}
		//2.计算分页参数
		int from = (current - 1) * SystemConstants.DEFAULT_PAGE_SIZE;
		int end = current * SystemConstants.DEFAULT_PAGE_SIZE;
		//3.查询redis，按照距离排序、分页。结果：shopId，distance
		//geo查询只能从第一条开始，到第end条数据
		String key=SHOP_GEO_KEY+typeId;
		GeoResults<RedisGeoCommands.GeoLocation<String>> results = stringRedisTemplate.opsForGeo()//geosearch key（按类型分组的typeId） bylonlat（自己定一个点或是member里面的一个点） x y（自己定义一个点的经纬度） byradius（圆或是矩形） 10（圆半径） withdistance（带上距离）
				.search(key, GeoReference.fromCoordinate(x, y), new Distance(5000), RedisGeoCommands.GeoSearchCommandArgs.newGeoSearchArgs().includeDistance().limit(end));
		//4.解析出id
		//防止查不到
		if(results==null){
			return Result.ok(Collections.emptyList());
		}
		//从返回结果里面获取第一条到第end条该类型的店铺id
		List<GeoResult<RedisGeoCommands.GeoLocation<String>>> list = results.getContent();
		//如果查到最后一页的下一页，查询到的结果就会为空，sql语句就会出现SELECT id,name,type_id,images,area,address,x,y,avg_price,sold,comments,score,open_hours,create_time,update_time FROM tb_shop WHERE (id IN ()) order by field(id,)的错误，所以我们需要直接返回空
		if(list.size()<=from){
			//没有下一页
			return Result.ok(Collections.emptyList());
		}
		//4.1截取from~end部分
		//创建一个list用来收集需要的店铺id
		List<Long> ids=new ArrayList<>(list.size());
		//创建一个map，用来id和distance一一对应用
		Map<String,Distance> distanceMap=new HashMap<>(list.size());
		//跳过from条数据，然后获取其中的店铺信息（id和distance）
		list.stream().skip(from).forEach(result->{
			//4.2获取从第from条到第end条数据的店铺id
			String shopIdStr = result.getContent().getName();
			//把获取到的店铺id添加到我们所创建好的ids集合里面
			ids.add(Long.valueOf(shopIdStr));
			//4.3获取距离
			Distance distance = result.getDistance();
			//把店铺id和距离distance一一对应好，存入distanceMap
			distanceMap.put(shopIdStr,distance);
		});
		//5.根据id查询店铺，通过StrUtil.join用","分隔开，把ids变成idStr字符串
		String idStr= StrUtil.join(",",ids);
		//把店铺id存入shops集合， query().in()是无序的，所以我们需要用last进行补充，让他们按我们idStr里面的顺序进行查询，并存入shops集合中
		List<Shop> shops = query().in("id", ids).last("order by field(id," + idStr + ")").list();
		for (Shop shop : shops) {
			//遍历，通过id查询，查到店铺信息，把距离存入到shop中去
			shop.setDistance(distanceMap.get(shop.getId().toString()).getValue());
		}
		//6.返回
		return Result.ok(shops);
	}

	/* //创建线程池，用于逻辑过期时的开启独立线程，创建一个10个线程的线程池
	private static final ExecutorService CACHE_REBUILD_EXECUTOR = Executors.newFixedThreadPool(10); */

	/* //逻辑过期，解决缓存击穿问题
	public Shop queryWithLogicalExpire(Long id) {
		String key = CACHE_SHOP_KEY + id;
		//1.从redis查询商铺缓存
		String shopJson = stringRedisTemplate.opsForValue().get(key);
		//2.判断是否存在
		if (StrUtil.isBlank(shopJson)) {
			//3.redis缓存中不存在，直接返回null
			return null;
		}
		//4.如果缓存命中，需要先把json反序列化为对象
		RedisData redisData = JSONUtil.toBean(shopJson, RedisData.class);
		//把redis中Object类型的data店铺信息转成shop类型的店铺信息
		Shop shop = JSONUtil.toBean((JSONObject) redisData.getData(), Shop.class);
		LocalDateTime expireTime = redisData.getExpireTime();
		//5.判断逻辑是否过期
		//判断过期时间是否在当前时间之后
		if (expireTime.isAfter(LocalDateTime.now())) {
			//5.1如果逻辑未过期，直接返回店铺信息
			return shop;
		}
		//5.2如果逻辑已过期，需要缓存重建
		//6.缓存重建
		//6.1设置互斥锁
		String lockKey = LOCK_SHOP_KEY + id;
		boolean isLock = tryLock(lockKey);
		//6.2判断锁是否获取成功
		if (isLock) {
			//6.3成功，开启独立线程，实现缓存重建
			CACHE_REBUILD_EXECUTOR.submit(() -> {
				try {
					//重建缓存
					this.saveShop2Redis(id, 20L);
				} catch (Exception e) {
					throw new RuntimeException(e);
				} finally {
					//释放锁
					unlock(lockKey);
				}
			});
		}
		//6.4失败，返回过期的商品信息
		return shop;
	} */

/* 	//通过互斥锁，解决缓存击穿问题
	public Shop queryWithMutex(Long id) {
		String key = CACHE_SHOP_KEY + id;
		//1.从redis查询商铺缓存
		String shopJson = stringRedisTemplate.opsForValue().get(key);
		//2.判断是否存在
		if (StrUtil.isNotBlank(shopJson)) {
			//3.redis缓存中存在，直接返回
			return JSONUtil.toBean(shopJson, Shop.class);
		}
		//判断一下命中的是否是空值（因为为了防止缓存穿透问题，我们把没查到的店铺信息用空值存入缓存了，为了防止空值查数据库，我们应该判断是否是空值，如果是空，那就意味着店铺不存在，就直接返回，没必要去数据库中进行查询）
		if (shopJson != null) {
			//返回错误信息
			return null;
		}
		//4.实现缓存重建
		//4.1获取互斥锁
		String lockKey = LOCK_SHOP_KEY + id;//注意，互斥锁的key和缓存的key是不一样的
		Shop shop = null;
		try {
			boolean isLock = tryLock(lockKey);
			//4.2判断是否获取成功
			if (isLock) {
				//4.3如果失败，则休眠并重试
				Thread.sleep(50);
				return queryWithMutex(id);
			}
			//4.4成功，redis缓存中不存在，根据id查询数据库
			shop = getById(id);
			//模拟重建的延时，模拟并发查询线程，检验互斥锁的效果
			Thread.sleep(200);
			//5.数据库不存在，为了防止缓存穿透问题，我们应该把空值存入缓存，然后再返回错误信息
			if (shop == null) {
				stringRedisTemplate.opsForValue().set(key, "", CACHE_NULL_TTL, TimeUnit.MINUTES);
				return null;
			}
			//6.数据库中存在，把数据写入redis缓存
			stringRedisTemplate.opsForValue().set(key, JSONUtil.toJsonStr(shop), CACHE_SHOP_TTL, TimeUnit.MINUTES);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		} finally {
			//7.释放互斥锁
			unlock(lockKey);
		}
		//8.返回
		return shop;
	} */

	//封装缓存穿透，以后直接使用CacheClient里的工具类了
	/* public Shop queryWithPassThrough(Long id) {
		String key = CACHE_SHOP_KEY + id;
		//1.从redis查询商铺缓存
		String shopJson = stringRedisTemplate.opsForValue().get(key);
		//2.判断是否存在
		if (StrUtil.isNotBlank(shopJson)) {
			//3.redis缓存中存在，直接返回
			return JSONUtil.toBean(shopJson, Shop.class);
		}
		//判断一下命中的是否是空值（因为为了防止缓存穿透问题，我们把没查到的店铺信息用空值存入缓存了，为了防止空值查数据库，我们应该判断是否是空值，如果是空，那就意味着店铺不存在，就直接返回，没必要去数据库中进行查询）
		if (shopJson != null) {
			//返回错误信息
			return null;
		}
		//4.redis缓存中不存在，根据id查询数据库
		Shop shop = getById(id);
		//5.数据库不存在，为了防止缓存穿透问题，我们应该把空值存入缓存，然后再返回错误信息
		if (shop == null) {
			stringRedisTemplate.opsForValue().set(key, "", CACHE_NULL_TTL, TimeUnit.MINUTES);
			return null;
		}
		//6.数据库中存在，把数据写入redis缓存
		stringRedisTemplate.opsForValue().set(key, JSONUtil.toJsonStr(shop), CACHE_SHOP_TTL, TimeUnit.MINUTES);
		//7.返回
		return shop;
	} */

/* 	//获取互斥锁，防止缓存击穿问题
	private boolean tryLock(String key) {
		Boolean flag = stringRedisTemplate.opsForValue().setIfAbsent(key, "1", 10, TimeUnit.SECONDS);
		//直接返回会做拆箱的，拆箱的过程中有可能出现空指针错误的，如果是true则返回flag
		return BooleanUtil.isTrue(flag);
	}

	//释放互斥锁，防止缓存击穿问题
	private void unlock(String key) {
		stringRedisTemplate.delete(key);
	}

	//把shop添加到redis当中
	//给shop添加上逻辑过期时间
	public void saveShop2Redis(Long id, Long expireSeconds) throws InterruptedException {
		//1.查询店铺数据
		Shop shop = getById(id);
		//缓存重建延迟200ms（更新延时200ms，模拟并发情况）
		Thread.sleep(200);
		//2.封装逻辑过期时间
		RedisData redisData = new RedisData();
		//把shop数据set进redis
		redisData.setData(shop);
		//把shop的逻辑过期时间set进redis，当前时间+自己设定的时间多少秒
		redisData.setExpireTime(LocalDateTime.now().plusSeconds(expireSeconds));
		//3.写入redis
		stringRedisTemplate.opsForValue().set(CACHE_SHOP_KEY + id, JSONUtil.toJsonStr(redisData));
	} */

}
