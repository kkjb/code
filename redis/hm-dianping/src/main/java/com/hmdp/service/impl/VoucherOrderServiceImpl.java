package com.hmdp.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hmdp.dto.Result;
import com.hmdp.entity.VoucherOrder;
import com.hmdp.mapper.VoucherOrderMapper;
import com.hmdp.service.ISeckillVoucherService;
import com.hmdp.service.IVoucherOrderService;
import com.hmdp.utils.RedisIdWorker;
import com.hmdp.utils.UserHolder;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.aop.framework.AopContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.connection.stream.*;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.time.Duration;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 虎哥
 * @since 2021-12-22
 */
//集群模式下，或者是分布式系统里，有多个jvm的存在，每个jvm会有自己的锁，导致每一个锁都能有一个线程获取，并行运行导致的并发安全问题（一个id在多个服务器上并发下单，导致id相同，锁不同，就会有不同的字符串常量池，导致多扣减库存，导致并发安全问题），所以我们需要分布式锁
@Slf4j
@Service
public class VoucherOrderServiceImpl extends ServiceImpl<VoucherOrderMapper, VoucherOrder> implements IVoucherOrderService {

	@Resource
	private ISeckillVoucherService seckillVoucherService;

	@Resource
	private RedisIdWorker redisIdWorker;

	@Resource
	private StringRedisTemplate stringRedisTemplate;

	@Resource
	private RedissonClient redissonClient;

	//提前定义读取好lua脚本文件，避免产生io流，影响性能
	private static final DefaultRedisScript<Long> SECKILL_SCRIPT;

	//因为它是静态的，所以我们需要在静态代码块里给他初始化
	static {
		//可以直接传入，但由于传入方式是硬编码的方式，不建议，所以我们还是放文件里面，修改起来方便
		SECKILL_SCRIPT = new DefaultRedisScript<>();
		//指定脚本，设置脚本的位置，他会直接去resources文件目录下去找
		SECKILL_SCRIPT.setLocation(new ClassPathResource("seckill.lua"));
		//配置返回值
		SECKILL_SCRIPT.setResultType(Long.class);
	}

/* 	//阻塞队列，当一个线程尝试从这个队列里获取元素的时候，如果没有元素，这个线程就会被阻塞，直到队列中有元素，他才会被唤醒，并且获取元素
	//ArrayBlockingQueue<>(指定初始化的大小，队列的长度)利用数组实现的一个阻塞队列
	private BlockingQueue<VoucherOrder> ordersTasks = new ArrayBlockingQueue<>(1024 * 1024); */

	//创建一个线程池，用来不断的去执行下单任务
	private static final ExecutorService SECKILL_ORDER_EXECUTOR = Executors.newSingleThreadExecutor();

	//在对象创建后立即执行一些初始化操作，其中将VoucherOrderHandler对象提交给执行器进行任务处理。
	//类初始化以后去执行线程池
	@PostConstruct
	//@PostConstruct在当前类初始化完毕后执行
	private void init() {
		SECKILL_ORDER_EXECUTOR.submit(new VoucherOrderHandler());
	}

	//创建一个线程任务，执行异步下单，从消息队列中取出订单
	private class VoucherOrderHandler implements Runnable {

		String queueName = "stream.orders";

		//执行业务逻辑
		@Override
		public void run() {
			while (true) {
				try {
					//1.获取消息队列中的订单信息 xreadgroup group g1（组） c1（消费者） count 1（读取数量） block（要不要阻塞） 2000（等待时间） STREAMS stream.orders（消息队列名称） >（读取标示，>表示未消费的第一条）
					List<MapRecord<String, Object, Object>> list = stringRedisTemplate.opsForStream().read(
							Consumer.from("g1", "c1"),//组名和消费者名
							StreamReadOptions.empty().count(1).block(Duration.ofSeconds(2)),//读取的数量和阻塞时间
							StreamOffset.create(queueName, ReadOffset.lastConsumed())//队列名称和读取标示，ReadOffset.lastConsumed()代表>
					);
					//2.判断消息是否获取成功
					if (list == null || list.isEmpty()) {
						//如果获取失败，说明没有消息，继续下一次循环
						continue;
					}
					//订单信息返回
					/* 1) 1) "stream.orders"
					2) 1) 1) "1692368377346-0"
					2) 1) "k1"
					2) "v1"
					3) "k2"
					4) "v2" */
					//3.解析消息中的订单信息
					MapRecord<String, Object, Object> record = list.get(0);
					//取出其中的值
					Map<Object, Object> values = record.getValue();
					//拿到order对象
					VoucherOrder voucherOrder = BeanUtil.fillBeanWithMap(values, new VoucherOrder(), true);
					//4.如果消息获取成功，说明有消息，可以下单，创建订单
					handleVoucherOrder(voucherOrder);
					//5.ACK确认 sack stream.orders g1 id（1692368377346-0）
					stringRedisTemplate.opsForStream().acknowledge(queueName, "g1", record.getId());
				} catch (Exception e) {
					log.error("处理订单异常", e);
					handlePendingList();
				}
			}
		}

		//处理异常消息
		private void handlePendingList() {
			while (true) {
				try {
					//1.获取pending-list中的订单信息 xreadgroup group g1（组） c1（消费者） count 1（读取数量） STREAMS stream.orders（消息队列名称） 0（读取标示，0表示已消费未确认的第一条，是从pending-list中的第一个消息开始）
					List<MapRecord<String, Object, Object>> list = stringRedisTemplate.opsForStream().read(
							Consumer.from("g1", "c1"),//组名和消费者名
							StreamReadOptions.empty().count(1),//读取的数量和阻塞时间
							StreamOffset.create(queueName, ReadOffset.from("0"))//队列名称和读取标示，ReadOffset.lastConsumed()代表>
					);
					//2.判断消息是否获取成功
					if (list == null || list.isEmpty()) {
						//如果获取失败，说明pending-list中没有异常消息，结束循环，确保pending-list中的全部异常信息处理完毕
						break;
					}
					//订单信息返回
					/* 1) 1) "stream.orders"
					2) 1) 1) "1692368377346-0"
					2) 1) "k1"
					2) "v1"
					3) "k2"
					4) "v2" */
					//3.解析消息中的订单信息
					MapRecord<String, Object, Object> record = list.get(0);
					//取出其中的值
					Map<Object, Object> values = record.getValue();
					//拿到order对象
					VoucherOrder voucherOrder = BeanUtil.fillBeanWithMap(values, new VoucherOrder(), true);
					//4.如果消息获取成功，说明有消息，可以下单，创建订单
					handleVoucherOrder(voucherOrder);
					//5.ACK确认 sack stream.orders g1 id（1692368377346-0）
					stringRedisTemplate.opsForStream().acknowledge(queueName, "g1", record.getId());
				} catch (Exception e) {
					log.error("处理pending-list订单异常", e);
					//让他休眠20ms
					try {
						Thread.sleep(20);
					} catch (InterruptedException interruptedException) {
						interruptedException.printStackTrace();
					}
				}
			}
		}
	}
	/* //创建一个线程任务，执行异步下单，从阻塞队列中取出订单
	private class VoucherOrderHandler implements Runnable {

		//执行业务逻辑
		@Override
		public void run() {
			while (true) {
				try {
					//1.获取队列中的订单信息
					VoucherOrder voucherOrder = ordersTasks.take();
					//2.创建订单
					handleVoucherOrder(voucherOrder);
				} catch (Exception e) {
					log.error("处理订单异常", e);
				}
			}
		}
	}*/

	//处理订单
	private void handleVoucherOrder(VoucherOrder voucherOrder) {
		//1.获取用户
		Long userId = voucherOrder.getUserId();
		//2.创建锁对象（同一个用户下单才会被锁定）
		//使用redissonClient来获取锁对象，getLock是最普通的锁，然后指定锁名称
		RLock lock = redissonClient.getLock("lock:order:" + userId);
		//3.尝试获取锁，无参就是不等待，有参的情况下，参数分别是：获取锁的最大等待时间（期间会重试），锁自动释放时间，时间单位
		boolean isLock = lock.tryLock();
		//4.判断是否获取锁成功
		if (!isLock) {
			//获取锁失败，因为这里是异步处理，不需要返回给前端信息
			// return Result.fail("不允许重复下单");
			log.error("不允许重复下单");
			return;
		}
		try {
			//获取事务有关的代理对象，确保事务生效，AopContext.currentProxy()拿到当前对象的代理对象，因为是返回的是IVoucherOrderService，所以我们需要强转
			//因为这里是一个子线程，是没有办法从threadlocal取出数据的，所以是拿不到的，应该在主线程里获取
			//拿到事务的代理对象（事务）
			// IVoucherOrderService proxy = (IVoucherOrderService) AopContext.currentProxy();
			//事务要想生效，是因为spring对VoucherOrderServiceImpl做了动态代理，拿到代理对象，做事务处理
			//return createVoucherOrder(voucherId);相当于return this.createVoucherOrder(voucherId);没法对事务进行管理（this.createVoucherOrder(voucherId)拿到的是当前的对象，而不是代理对象，他是没有事务功能的，这个是spring事务失效的几个可能性之一）
			//return proxy.createVoucherOrder(voucherId);是带有事务的管理
			proxy.createVoucherOrder(voucherOrder);
		} finally {
			//释放锁
			lock.unlock();
		}
	}

	//把他变为全员变量，让后续可以使用（相当于把代理对象放到了当前类里）
	private IVoucherOrderService proxy;

	//秒杀判断，判断秒杀是否开始，秒杀是否结束，库存是否充足（基于redis的lua脚本进行下单资格判断，基于stream消息队列进行消息的发送）
	@Override
	public Result seckillVoucher(Long voucherId) {
		//获取用户
		Long userId = UserHolder.getUser().getId();
		//获取订单id
		long orderId = redisIdWorker.nextId("order");
		//1.执行lua脚本，尝试判读用户是否有购买资格，脚本执行完，确保有购买资格，并且消息已经发出
		//调用lua脚本，不要写死lua脚本，未来可能对其进行修改
		Long result = stringRedisTemplate.execute(
				SECKILL_SCRIPT,//配置好的lua文件
				Collections.emptyList(),//所需要的key的集合，Collections.emptyList()代表空集合
				voucherId.toString(), userId.toString(), String.valueOf(orderId));//其他参数
		//2.判断结果是否为0
		//先把Long类型的转成int类型的
		int r = result.intValue();
		if (r != 0) {
			//2.1不为0，代表没有购买资格
			return Result.fail(r == 1 ? "库存不足" : "不能重复下单");
		}
		//3.获取代理对象
		//拿到事务的代理对象（事务）
		proxy = (IVoucherOrderService) AopContext.currentProxy();
		//3.返回订单id
		return Result.ok(orderId);
	}
/* 	//秒杀判断，判断秒杀是否开始，秒杀是否结束，库存是否充足（基于redis的lua脚本进行下单资格判断，基于阻塞队列进行消息的发送）
	@Override
	public Result seckillVoucher(Long voucherId) {
		//获取用户
		Long userId = UserHolder.getUser().getId();
		//1.执行lua脚本，尝试判读用户是否有购买资格
		//调用lua脚本，不要写死lua脚本，未来可能对其进行修改
		Long result = stringRedisTemplate.execute(
				SECKILL_SCRIPT,//配置好的lua文件
				Collections.emptyList(),//所需要的key的集合，Collections.emptyList()代表空集合
				voucherId.toString(), userId.toString());//其他参数
		//2.判断结果是否为0
		//先把Long类型的转成int类型的
		int r = result.intValue();
		if (r != 0) {
			//2.1不为0，代表没有购买资格
			return Result.fail(r == 1 ? "库存不足" : "不能重复下单");
		}
		//2.2为0，有购买资格，把下单信息保存到阻塞队列
		//创建订单
		VoucherOrder voucherOrder = new VoucherOrder();
		//2.3订单id
		long orderId = redisIdWorker.nextId("order");
		voucherOrder.setId(orderId);
		//2.4用户id（因为我们有登录拦截器，所以拦截器里面有用户信息）
		voucherOrder.setUserId(userId);
		//2.5代金券id（前端传来的代金券id）
		voucherOrder.setVoucherId(voucherId);
		//2.6放入阻塞队列
		ordersTasks.add(voucherOrder);
		//3.获取代理对象
		//拿到事务的代理对象（事务）
		proxy = (IVoucherOrderService) AopContext.currentProxy();
		//3.返回订单id
		return Result.ok(orderId);
	} */

	//秒杀判断，判断秒杀是否开始，秒杀是否结束，库存是否充足（基于mysql进行下单资格判断）
	/*
	 * 秒杀业务
	 * @Param 正在抢购的优惠券id
	 * @Return 成功返回
	 */
/* 	@Override
	public Result seckillVoucher(Long voucherId) {
		//1.查询优惠券
		SeckillVoucher voucher = seckillVoucherService.getById(voucherId);
		//2.判断秒杀是否开始
		//秒杀开始时间在现在时间之后
		if (voucher.getBeginTime().isAfter(LocalDateTime.now())) {
			//尚未开始
			return Result.fail("秒杀尚未开始!");
		}
		//3.判断秒杀是否已经结束
		//秒杀结束时间在现在时间之前
		if (voucher.getEndTime().isBefore(LocalDateTime.now())) {
			//已经结束
			return Result.fail("秒杀已经结束!");
		}
		//4.判断库存是否充足
		//判断库存是否小于1
		if (voucher.getStock() < 1) {
			//库存不足
			return Result.fail("库存不足!");
		}

		//单机启动的时候使用
		//一个用户用一把锁，把锁定范围减小
		//要保证id的值一样（.intern()添加到字符串池，不管你new了多少个，只要你的值是一样的，返回的结果也是一样的，确保用户的id一样时，锁就一样，不同的用户就不会被锁定，锁的性能就会提升了）
		//把整个事务锁起来，防止方法执行完，spring尚未提交其他线程并发导致的线程并发的安全问题
		//先获取锁，然后再进入createVoucherOrder函数，去完成下单操作，函数执行完了，就说明新的订单一定写入数据库了，事务已经提交完毕了，事务提交完才释放锁，确保了数据库内已经有订单了，就不会出现线程
		//先获取，提交事务，再释放锁，这样才能确保线程安全（避免事务没提交就释放锁的安全问题）
		Long userId = UserHolder.getUser().getId();
		synchronized (userId.toString().intern()) {
			//获取事务有关的代理对象，确保事务生效，AopContext.currentProxy()拿到当前对象的代理对象，因为是返回的是IVoucherOrderService，所以我们需要强转
			//拿到事务的代理对象
			IVoucherOrderService proxy = (IVoucherOrderService) AopContext.currentProxy();
			//事务要想生效，是因为spring对VoucherOrderServiceImpl做了动态代理，拿到代理对象，做事务处理
			//return createVoucherOrder(voucherId);相当于return this.createVoucherOrder(voucherId);没法对事务进行管理（this.createVoucherOrder(voucherId)拿到的是当前的对象，而不是代理对象，他是没有事务功能的，这个是spring事务失效的几个可能性之一）
			//return proxy.createVoucherOrder(voucherId);是带有事务的管理
			return proxy.createVoucherOrder(voucherId);
		}

		//如果库存充足
		Long userId = UserHolder.getUser().getId();
		//创建锁对象（同一个用户下单才会被锁定）
		// SimpleRedisLock lock = new SimpleRedisLock("order:" + userId, stringRedisTemplate);
		//使用redissonClient来获取锁对象，getLock是最普通的锁，然后指定锁名称
		RLock lock = redissonClient.getLock("lock:order:" + userId);
		//尝试获取锁，1200s后超时释放
		// boolean isLock = lock.tryLock(1200);
		//尝试获取锁，无参就是不等待，有参的情况下，参数分别是：获取锁的最大等待时间（期间会重试），锁自动释放时间，时间单位
		boolean isLock = lock.tryLock();
		//判断是否获取锁成功
		if (!isLock){
			//获取锁失败，返回错误信息或重试
			return Result.fail("不允许重复下单");
		}
		try {
			//获取事务有关的代理对象，确保事务生效，AopContext.currentProxy()拿到当前对象的代理对象，因为是返回的是IVoucherOrderService，所以我们需要强转
			//拿到事务的代理对象（事务）
			IVoucherOrderService proxy = (IVoucherOrderService) AopContext.currentProxy();
			//事务要想生效，是因为spring对VoucherOrderServiceImpl做了动态代理，拿到代理对象，做事务处理
			//return createVoucherOrder(voucherId);相当于return this.createVoucherOrder(voucherId);没法对事务进行管理（this.createVoucherOrder(voucherId)拿到的是当前的对象，而不是代理对象，他是没有事务功能的，这个是spring事务失效的几个可能性之一）
			//return proxy.createVoucherOrder(voucherId);是带有事务的管理
			return proxy.createVoucherOrder(voucherId);
		} finally {
			//释放锁
			lock.unlock();
		}
	} */
	//扣减库存，创建订单
	@Transactional
	//加上事务，一旦出现问题，可以及时回滚，因为这里是对数据库数据进行操作，用事务比较安全
	public void createVoucherOrder(VoucherOrder voucherOrder) {
		//5.一人一单判断，一个用户只能购买一张该优惠券（一人一单也会出现并发问题）
		//因为一人一单是往数据库的订单表内做插入操作，而不是修改操作，不能用乐观锁来判断数据是否修改，因此，我们只能用悲观锁来解决一人一单的问题
		Long userId = voucherOrder.getUserId();
		//5.1查询订单（这里不需要查出具体的订单，只需要查到用户id和优惠券id相同的订单数量就行了）
		int count = query().eq("user_id", userId).eq("voucher_id", voucherOrder.getVoucherId()).count();
		//5.2判断用户id和优惠券id是否存在（如果查到的用户id和优惠券id相同的订单数量大于0，证明用户至少下过一单）
		if (count > 0) {
			//用户已经购买过了
			log.error("用户已经购买过一次！");
			return;
		}
		//如果查到的用户id和优惠券id相同的订单数量等于0，那说明该用户还没购买过这张优惠券，则接下去扣减库存，创建订单
		//6.扣减库存
		//使用手动setsql的方式，把stock-1赋值到对应的voucherId里去
		//使用乐观锁来解决超卖问题，查看并发时对应的库存数量是否准确，进行判断，所以应该在修改数据之前，对库存量进行判断
		//因为多个线程并发执行时，第一个线程一旦修改成功，乐观锁就会因为库存产生变化，使剩下并发的线程修改失败
		//为了解决乐观锁失败率高的问题，我们可以对其进行修改，不用要求库存必须和之前保持一致，只需要要求库存大于0就行了
		boolean success = seckillVoucherService.update()
				.setSql("stock = stock - 1 ")//set stock = stock - 1
				.eq("voucher_id", voucherOrder.getVoucherId())
				// .eq("stock",voucher.getStock())//where id = ? and stock = ?（stock是乐观锁的判断，更新的stock值要和查询到的stock值一致，证明在此之前没有人对数据进行修改，解决线程并发问题）
				.gt("stock", 0)//解决乐观锁失败率高的问题，stock>0
				.update();
		if (!success) {
			//扣除失败，一般可能是库存不足导致的，所以我们也可以直接返回库存不足的提示
			log.error("库存不足!");
			return;
		}
		save(voucherOrder);
	}
    /*@Transactional
    public Result createVoucherOrder(Long voucherId) {
        // 5.一人一单
        Long userId = UserHolder.getUser().getId();

        synchronized (userId.toString().intern()) {
            // 5.1.查询订单
            int count = query().eq("user_id", userId).eq("voucher_id", voucherId).count();
            // 5.2.判断是否存在
            if (count > 0) {
                // 用户已经购买过了
                return Result.fail("用户已经购买过一次！");
            }
            // 6.扣减库存
            boolean success = seckillVoucherService.update()
                    .setSql("stock = stock - 1") // set stock = stock - 1
                    .eq("voucher_id", voucherId).gt("stock", 0) // where id = ? and stock > 0
                    .update();
            if (!success) {
                // 扣减失败
                return Result.fail("库存不足！");
            }
            // 7.创建订单
            VoucherOrder voucherOrder = new VoucherOrder();
            // 7.1.订单id
            long orderId = redisIdWorker.nextId("order");
            voucherOrder.setId(orderId);
            // 7.2.用户id
            voucherOrder.setUserId(userId);
            // 7.3.代金券id
            voucherOrder.setVoucherId(voucherId);
            save(voucherOrder);
            // 7.返回订单id
            return Result.ok(orderId);
        }
    }*/
}
