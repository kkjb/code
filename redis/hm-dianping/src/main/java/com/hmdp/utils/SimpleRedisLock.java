package com.hmdp.utils;

import cn.hutool.core.lang.UUID;
import com.hmdp.service.ILock;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;

import java.util.Collections;
import java.util.concurrent.TimeUnit;

/**
 * @ClassName SimpleRedisLock
 * @Description 实现ILock类
 * @Author 五更琉璃
 * @LastChangeDate 2023/8/6 21:51
 */

//线程1因为业务阻塞，导致超时释放锁，在业务完成后把正在执行业务的线程2的锁释放了（redis分布式锁误删的问题），所以应该在释放锁的时候，获取锁标识并判断是否一致（判断线程id是否一致）
//线程1判断锁标示是否是自己和释放锁之间产生了阻塞，导致超时释放锁，在业务完成后把正在执行业务的线程2的锁释放了，所以判断锁操作和释放锁操作要成为原子性，一起执行，避免出现间隔（使用lua脚本，使判断锁标示是否一致和释放锁的操作同时执行，确保了原子性）
public class SimpleRedisLock implements ILock {

	//不同的业务有不同的锁的名称
	private String name;
	private StringRedisTemplate stringRedisTemplate;

	//这两个参数由用户传递给我们的，所以我们需要创建一个构造函数用来给他们赋值，所以用一个构造函数用来接收这两个参数
	public SimpleRedisLock(String name, StringRedisTemplate stringRedisTemplate) {
		this.name = name;
		this.stringRedisTemplate = stringRedisTemplate;
	}

	//锁名的前缀，相当于key前缀
	private static final String KEY_PREFIX ="lock:";
	//线程前缀toString(true)代表把uuid里面的横线去掉，相当于value前缀，加上线程前缀，避免重复
	private static final String ID_PREFIX = UUID.randomUUID().toString(true)+"-";
	//提前定义读取好lua脚本文件，避免产生io流，影响性能
	private static final DefaultRedisScript<Long> UNLOCK_SCRIPT;
	//因为它是静态的，所以我们需要在静态代码块里给他初始化
	static {
		//可以直接传入，但由于传入方式是硬编码的方式，不建议，所以我们还是放文件里面，修改起来方便
		UNLOCK_SCRIPT=new DefaultRedisScript<>();
		//指定脚本，设置脚本的位置，他会直接去resources文件目录下去找
		UNLOCK_SCRIPT.setLocation(new ClassPathResource("unlock.lua"));
		//配置返回值
		UNLOCK_SCRIPT.setResultType(Long.class);
	}
	//获取锁
	@Override
	public boolean tryLock(long timeoutSec) {
		//获取当前线程的id（获取当前线程的标识），value整体
		String threadId =ID_PREFIX+Thread.currentThread().getId();
		//获取锁，例如set lock thread1 ex 10 nx（lock锁的key，thread锁的value，ex 10代表过期时间10s，nx代表互斥（setnx lock thread1），redis数据库中只能有一个这个key）
		//setIfAbsent()代表如果不存在才执行就是setnx，threadId+"" 表示将 threadId 转换为字符串类型，因为String threadId =ID_PREFIX+Thread.currentThread().getId();所以线程id已经变成字符串了，就可以去掉+""了
		Boolean success = stringRedisTemplate.opsForValue().setIfAbsent(KEY_PREFIX+name, threadId, timeoutSec, TimeUnit.SECONDS);
		//Boolean是个包装类，而我们返回的boolean是个基本类型，包装类转成基本类型需要做拆箱，有可能导致安全问题，当success是null的时候，直接返回就会导致空指针问题，所以我们需要用Boolean是个包装类和success进行去比较，如果true就返回true，如果是false或null就返回false，避免空指针的可能性
		return Boolean.TRUE.equals(success);
	}
	//释放锁
/* 	@Override
	public void unlock() {
		//获取线程标示，获取之前获取锁时候的线程的value
		String threadId =ID_PREFIX+Thread.currentThread().getId();
		//获取锁中的标示，获取redis中的value
		String id = stringRedisTemplate.opsForValue().get(KEY_PREFIX + name);
		//判断标示是否一致，如果一致就删除，如果不一致就跳过
		if (threadId.equals(id)){
			//释放锁
			stringRedisTemplate.delete(KEY_PREFIX+name);
		}
	} */
	//基于lua脚本执行释放锁的操作
	public void unlock(){
		//调用lua脚本，不要写死lua脚本，未来可能对其进行修改
		stringRedisTemplate.execute(
				UNLOCK_SCRIPT,//配置好的lua文件
				Collections.singletonList(KEY_PREFIX+name),//key的集合
				ID_PREFIX+Thread.currentThread().getId());//其他参数，也就是线程标示
	}

}
