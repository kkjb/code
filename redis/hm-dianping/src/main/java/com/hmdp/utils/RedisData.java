package com.hmdp.utils;

import lombok.Data;

import java.time.LocalDateTime;

//存进redis的数据
@Data
public class RedisData {
    private LocalDateTime expireTime;//逻辑过期时间
    private Object data;//存进redis的数据，可以不对原来的实体类进行修改
}
