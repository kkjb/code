package com.hmdp.utils;

import cn.hutool.core.util.BooleanUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.hmdp.entity.Shop;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.applet.Applet;
import java.time.LocalDateTime;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import static com.hmdp.utils.RedisConstants.*;

/**
 * @author: 五更琉璃
 * @date: 2023/7/24 21:40
 */
@Slf4j
@Component
public class CacheClient {
	private final StringRedisTemplate stringRedisTemplate;
	public CacheClient(StringRedisTemplate stringRedisTemplate){
		this.stringRedisTemplate=stringRedisTemplate;
	}
	//存入缓存
	public void set(String key, Object value, Long time, TimeUnit unit){
		stringRedisTemplate.opsForValue().set(key, JSONUtil.toJsonStr(value),time,unit);
	}
	//用于逻辑过期解决缓存击穿
	public void setWithLogicalExpire(String key, Object value, Long time, TimeUnit unit){
		//设置逻辑过期
		//新建一个RedisData用于存储
		RedisData redisData = new RedisData();
		//往redisData存入值
		redisData.setData(value);
		//往redisData存入逻辑时间（当前时间+传来的时间，要把unit转换成秒进行计算）
		redisData.setExpireTime(LocalDateTime.now().plusSeconds(unit.toSeconds(time)));
		//写入redis
		stringRedisTemplate.opsForValue().set(key, JSONUtil.toJsonStr(redisData));
	}
	//封装缓存穿透
	//当返回的类型不确定时，应该使用泛型，Class<R> type返回的具体类型，到时候返回对应的类型（泛型推断）
	//keyPrefix代表key的前缀，最终的key是由keyPrefix和id组合起来的，id的类型不确定，所以我们也需要用泛型来代表id的类型
	//去数据库查询的过程交给调用者来做，我们这边封装查询的逻辑，有参有返回值，我们应该使用Function<参数的类型，返回值类型>，类似于R r = getById(id);
	//Long time, TimeUnit unit时间和其对应的单位
	public <R,ID> R queryWithPassThrough(String keyPrefix, ID id, Class<R> type, Function<ID,R> dbFallback,Long time, TimeUnit unit) {
		String key = keyPrefix + id;
		//1.从redis查询缓存
		String json = stringRedisTemplate.opsForValue().get(key);
		//2.判断是否存在
		if (StrUtil.isNotBlank(json)) {
			//3.redis缓存中存在，直接返回R类型type
			return JSONUtil.toBean(json, type);
		}
		//判断一下命中的是否是空值（因为为了防止缓存穿透问题，我们把没查到的东西用空值存入缓存了，为了防止空值查数据库，我们应该判断是否是空值，如果是空，那就意味着该东西不存在，就直接返回，没必要去数据库中进行查询）
		if (json != null) {
			//返回错误信息
			return null;
		}
		//4.redis缓存中不存在，根据id查询数据库
		R r = dbFallback.apply(id);
		//5.数据库不存在，为了防止缓存穿透问题，我们应该把空值存入缓存，然后再返回错误信息
		if (r == null) {
			stringRedisTemplate.opsForValue().set(key, "", CACHE_NULL_TTL, TimeUnit.MINUTES);
			return null;
		}
		//6.数据库中存在，把数据写入redis缓存
		this.set(key,r,time,unit);
		//7.返回
		return r;
	}

	//创建线程池，用于逻辑过期时的开启独立线程，创建一个10个线程的线程池
	private static final ExecutorService CACHE_REBUILD_EXECUTOR = Executors.newFixedThreadPool(10);
	//逻辑过期，解决缓存击穿问题
	public <R,ID> R queryWithLogicalExpire(String keyPrefix,ID id,Class<R> type,Function<ID,R> dbFallback,Long time, TimeUnit unit) {
		String key = keyPrefix + id;
		//1.从redis查询商铺缓存
		String json = stringRedisTemplate.opsForValue().get(key);
		//2.判断是否存在
		if (StrUtil.isBlank(json)) {
			//3.redis缓存中不存在，直接返回null
			return null;
		}
		//4.如果缓存命中，需要先把json反序列化为对象
		RedisData redisData = JSONUtil.toBean(json, RedisData.class);
		//把redis中Object类型的data店铺信息转成shop类型的店铺信息
		R r = JSONUtil.toBean((JSONObject) redisData.getData(), type);
		LocalDateTime expireTime = redisData.getExpireTime();
		//5.判断逻辑是否过期
		//判断过期时间是否在当前时间之后
		if (expireTime.isAfter(LocalDateTime.now())) {
			//5.1如果逻辑未过期，直接返回店铺信息
			return r;
		}
		//5.2如果逻辑已过期，需要缓存重建
		//6.缓存重建
		//6.1设置互斥锁
		String lockKey = LOCK_SHOP_KEY + id;
		boolean isLock = tryLock(lockKey);
		//6.2判断锁是否获取成功
		if (isLock) {
			//6.3成功，开启独立线程，实现缓存重建
			CACHE_REBUILD_EXECUTOR.submit(() -> {
				try {
					//重建缓存
					//先查数据库
					R r1 = dbFallback.apply(id);
					//写入redis
					this.setWithLogicalExpire(key, r1,time,unit);
				} catch (Exception e) {
					throw new RuntimeException(e);
				} finally {
					//释放锁
					unlock(lockKey);
				}
			});
		}
		//6.4失败，返回过期的商品信息
		return r;
	}
	//通过互斥锁，解决缓存击穿问题
	public <R,ID> R queryWithMutex(String keyPrefix,ID id,Class<R> type,Function<ID,R> dbFallback,Long time, TimeUnit unit) {
		String key = keyPrefix + id;
		//1.从redis查询商铺缓存
		String json = stringRedisTemplate.opsForValue().get(key);
		//2.判断是否存在
		if (StrUtil.isNotBlank(json)) {
			//3.redis缓存中存在，直接返回
			return JSONUtil.toBean(json,type);
		}
		//判断一下命中的是否是空值（因为为了防止缓存穿透问题，我们把没查到的店铺信息用空值存入缓存了，为了防止空值查数据库，我们应该判断是否是空值，如果是空，那就意味着店铺不存在，就直接返回，没必要去数据库中进行查询）
		if (json != null) {
			//返回错误信息
			return null;
		}
		//4.实现缓存重建
		//4.1获取互斥锁
		String lockKey = LOCK_SHOP_KEY + id;//注意，互斥锁的key和缓存的key是不一样的
		R r = null;
		try {
			boolean isLock = tryLock(lockKey);
			//4.2判断是否获取成功
			if (isLock) {
				//4.3如果失败，则休眠并重试
				return queryWithMutex(keyPrefix,id,type,dbFallback,50L,TimeUnit.MILLISECONDS);
			}
			//4.4成功，redis缓存中不存在，根据id查询数据库
			r = dbFallback.apply(id);
			//模拟重建的延时，模拟并发查询线程，检验互斥锁的效果
			//5.数据库不存在，为了防止缓存穿透问题，我们应该把空值存入缓存，然后再返回错误信息
			if (r == null) {
				stringRedisTemplate.opsForValue().set(key, "", CACHE_NULL_TTL, TimeUnit.MINUTES);
				return null;
			}
			//6.数据库中存在，把数据写入redis缓存
			this.setWithLogicalExpire(key, JSONUtil.toJsonStr(r), time, unit);
		} finally {
			//7.释放互斥锁
			unlock(lockKey);
		}
		//8.返回
		return r;
	}
	//获取互斥锁，防止缓存击穿问题
	private boolean tryLock(String key) {
		Boolean flag = stringRedisTemplate.opsForValue().setIfAbsent(key, "1", 10, TimeUnit.SECONDS);
		//直接返回会做拆箱的，拆箱的过程中有可能出现空指针错误的，如果是true则返回flag
		return BooleanUtil.isTrue(flag);
	}

	//释放互斥锁，防止缓存击穿问题
	private void unlock(String key) {
		stringRedisTemplate.delete(key);
	}

}
