package com.hmdp.utils;

import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

/**
 * @author: 五更琉璃
 * @date: 2023/7/26 18:37
 */
//全局唯一id：redis自增（生成redis的自增id）
@Component
public class RedisIdWorker {

	@Resource
	private StringRedisTemplate stringRedisTemplate;

	//开始时间戳
	private static final long BEGIN_TIMESTAMP = 1640995200L;

	//因为我们需要把时间戳和序列号进行拼接，所以我们需要把时间戳向左移动32位，COUNT_BITS就是时间戳向左移动的位数，也是序列号的位数
	private static final int COUNT_BITS = 32;

	//id的自增长，传入业务的前缀
	public long nextId(String keyPrefix) {
		//全局唯一id格式：0-00000000 00000000 00000000 0000000 - 00000000 00000000 00000000 00000000
		//第一个是符号位，中间31位是时间戳（31 bit），后面32位是序列号（32 bit）
		//1.生成时间戳
		//生成当前的时间
		LocalDateTime now = LocalDateTime.now();
		//把当前的时间转化成当前的秒数
		long nowSecond = now.toEpochSecond(ZoneOffset.UTC);
		long timestamp = nowSecond - BEGIN_TIMESTAMP;
		//2.生成序列号，利用redis的自增长
		//2.1获取当前的日期，精确到天，防止redis自增长过大，以天作为key还具备了统计订单量的效果
		String date = now.format(DateTimeFormatter.ofPattern("yyyyMMdd"));
		//count的数据类型要用基本类型long，而不能使用包装类型Long，因为需要做计算
		//因为订单号是从新的一天的开始的，所以不会产生空指针
		long count = stringRedisTemplate.opsForValue().increment("icr:" + keyPrefix + ":" + date);
		//3.拼接并返回，因为是二进制运算，所以需要采用左移的方式和或运算
		return timestamp << COUNT_BITS | count;
	}

}
