package com.hmdp.utils;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.hmdp.dto.UserDTO;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author: 五更琉璃
 * @date: 2023/7/8 10:52
 */
public class RefreshTokenInterceptor implements HandlerInterceptor {

	// @Override
	// public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
	// 	//1.获取session
	// 	HttpSession session = request.getSession();
	// 	//2.获取session中的用户
	// 	Object user = session.getAttribute("user");
	// 	//3.判断用户是否存在
	// 	if(user == null){
	// 		//4.不存在，拦截，返回401状态码
	// 		response.setStatus(401);
	// 		return false;
	// 	}
	// 	//5.存在，保存用户信息到ThreadLocal
	// 	UserHolder.saveUser((UserDTO) user);
	// 	//6.放行
	// 	return true;
	// }

	//这个类不是由spring创建的，而是由我们自己创建的，所以我们应该自己手动做依赖注入
	private StringRedisTemplate stringRedisTemplate;
	public RefreshTokenInterceptor(StringRedisTemplate stringRedisTemplate) {
	    this.stringRedisTemplate = stringRedisTemplate;
	}
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		//1.获取请求头中的token
		String token = request.getHeader("authorization");
		//判断token是否为空
		if(StrUtil.isBlank(token)){
			return true;
		}
		//2.基于token获取redis中的用户
		String key=RedisConstants.LOGIN_USER_KEY + token;
		Map<Object, Object> userMap = stringRedisTemplate.opsForHash().entries(key);
		//3.判断用户是否存在
		if(userMap.isEmpty()){
			return true;
		}
		//5.将查询到的hash数据转换成userDTO对象（用一个map来填充bean，填充哪个bean，要不要忽略转化过程中的错误）
		UserDTO userDTO = BeanUtil.fillBeanWithMap(userMap, new UserDTO(), false);
		//6.存在，保存用户信息到ThreadLocal
		UserHolder.saveUser(userDTO);
		//7.刷新token有效期
		// stringRedisTemplate.expire(key,RedisConstants.LOGIN_USER_TTL, TimeUnit.MINUTES);
		//避免token过期，改长时间
		stringRedisTemplate.expire(key,114514L, TimeUnit.DAYS);
		//8.放行
		return true;
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
		//移除用户
		UserHolder.removeUser();
	}

}
