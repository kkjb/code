package com.hmdp.config;

import com.hmdp.utils.LoginInterceptor;
import com.hmdp.utils.RefreshTokenInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;

/**
 * @author: 五更琉璃
 * @date: 2023/7/8 14:29
 */
//配置拦截器，继承WebMvcConfigurer
@Configuration
public class MvcConfig implements WebMvcConfigurer {
	//这边添加了@Configuration注解，说明这个类将来由spring来构建这个类，所以它可以依赖注入，所以可以使用@Resource注入StringRedisTemplate
	@Resource
	private StringRedisTemplate stringRedisTemplate;
	//添加拦截器，把LoginInterceptor添加到拦截器
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		//LoginInterceptor登录拦截器拦部分请求
		registry.addInterceptor(new LoginInterceptor()).
				//需要放行的接口
				excludePathPatterns(
						"/shop/**",
						"/voucher/**",
						"/shop-type/**",
						"/upload/**",
						"/blog/hot",
						"/user/code",
						"/user/login"
						//order小的优先执行
				).order(1);
		//token刷新的拦截器，RefreshTokenInterceptor刷新的拦截器拦所有的请求,addPathPatterns("/**")拦所有
		registry.addInterceptor(new RefreshTokenInterceptor(stringRedisTemplate)).addPathPatterns("/**").order(0);
	}

}
