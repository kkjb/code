package com.hmdp.config;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName RedissonConfig
 * @Description redisson配置类
 * @Author 五更琉璃
 * @Date: 2023/8/12
 */
@Configuration
public class RedissonConfig {
	//声明成bean，方便去使用
	@Bean
	public RedissonClient redissonClient(){
		//配置
		Config config = new Config();
		// 添加redis地址，这里添加了单点的地址，也可以使用config.useClusterServers()添加集群地址，设置redis密码
		config.useSingleServer().setAddress("redis://192.168.186.133:6379").setPassword("123456");
		//创建RedissonClient对象
		return Redisson.create(config);
	}
}
