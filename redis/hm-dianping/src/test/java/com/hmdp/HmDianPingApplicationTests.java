package com.hmdp;

import com.hmdp.entity.Shop;
import com.hmdp.service.impl.ShopServiceImpl;
import com.hmdp.utils.CacheClient;
import com.hmdp.utils.RedisIdWorker;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.geo.Point;
import org.springframework.data.redis.connection.RedisGeoCommands;
import org.springframework.data.redis.core.StringRedisTemplate;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static com.hmdp.utils.RedisConstants.CACHE_SHOP_KEY;
import static com.hmdp.utils.RedisConstants.SHOP_GEO_KEY;

@SpringBootTest
class HmDianPingApplicationTests {

	@Resource
	private ShopServiceImpl shopService;

	@Resource
	private CacheClient cacheClient;

	@Resource
	private RedisIdWorker redisIdWorker;

	@Resource
	private StringRedisTemplate stringRedisTemplate;

	private ExecutorService es = Executors.newFixedThreadPool(500);

	//检测id并发添加的情况
	@Test
	void testIdWorker() throws InterruptedException {
		//等待300个任务执行完毕
		CountDownLatch latch = new CountDownLatch(300);
		//执行任务
		Runnable task = () -> {
			//每个线程来了都去生成一个id，并把它打印出来
			for (int i = 0; i < 100; i++) {
				long id = redisIdWorker.nextId("order");
				System.out.println("id = " + id);
			}
		};
		//记录任务开始时间
		long begin = System.currentTimeMillis();
		//提交任务300次
		for (int i = 0; i < 300; i++) {
			es.submit(task);
			//300*100=30000,相当于30000个id
		}
		//等待300个任务执行完毕，然后才去记录时间
		latch.await();
		//记录任务结束时间
		long end = System.currentTimeMillis();
		System.out.println("time = " + (end - begin));
	}

	//检测添加数据进缓存的情况
	@Test
	void testSaveShop() throws InterruptedException {
		Shop shop = shopService.getById(1L);
		cacheClient.setWithLogicalExpire(CACHE_SHOP_KEY + 1l, shop, 10L, TimeUnit.SECONDS);
	}

	//插入店铺信息和经纬度到redis
	@Test
	void loadShopData() {
		//1.查询店铺信息
		List<Shop> list = shopService.list();
		//2.把店铺按typeId分组，id一致的放到一个集合，Long是typeId，List<Shop>是店铺信息
		Map<Long, List<Shop>> map = list.stream().collect(Collectors.groupingBy(Shop::getTypeId));
		//3.分批完成写入redis
		for (Map.Entry<Long, List<Shop>> entry : map.entrySet()) {
			//3.1获取类型id
			Long typeId = entry.getKey();
			String key = SHOP_GEO_KEY + typeId;
			//3.2获取同类型的店铺集合
			List<Shop> value = entry.getValue();
			//创建一个locations组，用来存放member的经纬度
			List<RedisGeoCommands.GeoLocation<String>> locations = new ArrayList<>(value.size());
			//3.3写入redis geoadd key 经度 维度 member
			for (Shop shop : value) {
				// stringRedisTemplate.opsForGeo().add(key,new Point(shop.getX(),shop.getY()),shop.getId().toString());
				//往locations添加信息，member，经度，纬度（我们只需要把店铺id存进去就行了）
				locations.add(new RedisGeoCommands.GeoLocation<>(shop.getId().toString(), new Point(shop.getX(), shop.getY())));
			}
			//往redis中存入经纬度信息（key是类型id，value是locations组包含了店铺id，经度，纬度）
			stringRedisTemplate.opsForGeo().add(key, locations);
		}
	}
	//测试uv统计的内存占用
	@Test
	void testHyperLogLog(){
		String[] values=new String[1000];
		int j=0;
		for(int i=0;i<1000000;i++){
			//创建一个j，j只会0~999，这样防止数组角标溢出，分批发送
			j=i%1000;
			values[j]="user_"+i;
			if(j==999){
				//发送到redis，每个1000条发送一次
				stringRedisTemplate.opsForHyperLogLog().add("hl2",values);
			}
		}
		//统计数量
		Long count = stringRedisTemplate.opsForHyperLogLog().size("hl2");
		System.out.println("count="+count);
	}

}
