package com.heima.redis.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;

/**
 * @author: 五更琉璃
 * @date: 2023/7/5 18:26
 */
@Configuration
public class RedisConfig {
	//注册成一个bean，key为string类型，value为Object类型的，RedisTemplate的创建需要连接工厂，但是这个连接工厂不需要我们去创建，会由springboot自动给我们创建，我们只需要注入进去就行了
	@Bean
	public RedisTemplate<String,Object> redisTemplate(RedisConnectionFactory connectionFactory) {
	    //创建RedisTemplate对象，key为string类型，value为Object类型的
		RedisTemplate<String,Object> template = new RedisTemplate<>();;
		//设置连接工厂
		template.setConnectionFactory(connectionFactory);
		//创建JSON序列化工具
		GenericJackson2JsonRedisSerializer jsonRedisSerializer = new GenericJackson2JsonRedisSerializer();
		//设置key的序列化，设置一个utf-8的常量，就不需要我们自己去创建了
		template.setKeySerializer(RedisSerializer.string());
		template.setHashKeySerializer(RedisSerializer.string());
		//设置value的序列化
		template.setValueSerializer(jsonRedisSerializer);
		template.setHashValueSerializer(jsonRedisSerializer);
		//返回
		return template;
	}
}
