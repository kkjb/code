package com.heima.redis.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author: 五更琉璃
 * @date: 2023/7/5 18:40
 */
@Data
//添加一个无参构造函数的注解
@NoArgsConstructor
//添加一个有参构造函数的注解
@AllArgsConstructor
public class User {
	private String name;
	private Integer age;

}
