package com.heima;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.heima.redis.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

//4.编写测试
@SpringBootTest
class RedisStringTests {
	@Autowired
	private StringRedisTemplate stringRedisTemplate;
	//String类型的value直接使用stringRedisTemplate进行序列化
	@Test
	void testString() {
		//写入一条String数据
		stringRedisTemplate.opsForValue().set("name","虎哥2");
		//获取string数据
		Object name = stringRedisTemplate.opsForValue().get("name");
		System.out.println("name = "+name);
	}
	//json类型的value需要手动序列化和手动反序列化
	//调用手动序列化工具
	private static final ObjectMapper mapper = new ObjectMapper();
	@Test
	void testSaveUser() throws JsonProcessingException {
		//创建对象
		User user = new User("虎哥3",21);
		//手动序列化，把他变成json
		String json = mapper.writeValueAsString(user);
		//写入对象
		stringRedisTemplate.opsForValue().set("user:200",json);
		//获取数据
		String jsonUser = stringRedisTemplate.opsForValue().get("user:200");
		//手动反序列化，把他变成user类型的数据
		User user1 = mapper.readValue(jsonUser, User.class);
		System.out.println("user1 = "+user1);
	}
	//hash表中一个一个添加数据
	@Test
	void testHash(){
		stringRedisTemplate.opsForHash().put("user:400","name","虎哥");
		stringRedisTemplate.opsForHash().put("user:400","age","21");
		Map<Object, Object> entries = stringRedisTemplate.opsForHash().entries("user:400");
		System.out.println("entries = "+entries);
	}
	//hash表中一次性添加多个数据
	@Test
	void testHashAll(){
		Map<String,String> data=new HashMap<>();
		data.put("name","虎哥");
		data.put("age","25");
		data.put("location","beijing");
		stringRedisTemplate.opsForHash().putAll("user:500", data);
		Map<Object, Object> entries2 = stringRedisTemplate.opsForHash().entries("user:500");
		System.out.println("entries2 = "+entries2);
	}
}
