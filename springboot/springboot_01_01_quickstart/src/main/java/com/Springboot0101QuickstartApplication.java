package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * springboot的引导类是boot工程的执行入口，运用main方法就可以启动项目
 * springboot工程运行后初始化spring容器，扫描引导所在包加载bean
 */
@SpringBootApplication
public class Springboot0101QuickstartApplication {

    public static void main(String[] args) {
        SpringApplication.run(Springboot0101QuickstartApplication.class, args);
    }

}
