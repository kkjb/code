package com.demo.dao;

import com.demo.domain.Book;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

// 3.定义数据层接口与映射配置
// 数据库sql映射需要添加@Mapper被容器识别到
@Mapper
public interface BookDao {
    @Select("select * from tbl_book where id = #{id}")
    public Book getById(Integer id);
}
