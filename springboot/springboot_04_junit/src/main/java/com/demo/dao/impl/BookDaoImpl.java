package com.demo.dao.impl;

import com.demo.dao.BookDao;
import org.springframework.stereotype.Repository;

@Repository
public class BookDaoImpl implements BookDao {
    @Override
    public void save(){
        System.out.println("book dao is running...");
    }
}
