package com;

import com.demo.dao.BookDao;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

//如果test的位置和引导类位置不同时必须加上@SpringBootTest(classes = Springboot04JunitApplicationTests.class)（精准的指定配置类）或使用@ContextConfiguration(classes = Springboot04JunitApplication.class)
@SpringBootTest
/**
 * 名称：@SpringBootTest
 * 类型：测试类注解
 * 位置：测试类定义的上方
 * 作用：设置JUnit加载的SpringBoot启动类
 * 相关属性：classes：设置SpringBoot的启动类
 * 注意事项：如果测试类在SpringBoot启动类的包或者子包中，可以省略启动类的配置，也就是省略classes的设定
 */
// @ContextConfiguration(classes = Springboot04JunitApplication.class)
class Springboot04JunitApplicationTests {
    //SpringBoot整合JUnit
    //1.注入你要测试的对象
    @Autowired
    private BookDao bookDao;
    @Test
    void contextLoads() {
        //2.执行要测试的对象对应的方法
        bookDao.save();
    }
}
