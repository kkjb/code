package com.example.controller;

import com.example.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/login")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping
    public String login(String username, String password) {
        if (userService.login(username, password)) {
            return "登录成功";
        }
            return "登录失败";

    }
}