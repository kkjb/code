package com.example.dao;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.domain.Book;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

//制作测试类测试结果
@SpringBootTest
public class BookDaoTestCase {
    @Autowired
    private BookDao bookDao;

    @Test
    void testGetById() {
        System.out.println(bookDao.selectById(1));
    }

    @Test
    void testSave() {
        Book book=new Book();
        book.setType("测试数据boot");
        book.setName("测试数据boot");
        book.setDescription("测试数据boot");
        bookDao.insert(book);
    }

    @Test
    void testUpdate() {
        Book book=new Book();
        book.setId(16);
        book.setType("测试数据boot123");
        book.setName("测试数据boot");
        book.setDescription("测试数据boot");
        bookDao.updateById(book);
    }

    @Test
    void testDelete() {
        bookDao.deleteById(14);
    }

    @Test
    void testGetAll() {
        bookDao.selectList(null);
    }

    //分页操作需要设定分页对象IPage
    @Test
    void testGetPage() {
        //select * from tbl_book limit 2,5
        /**
         * current：当前页码值，size：每页数据总量，total：数据总量，page：最大页码值，records：数据
         */
        IPage page=new Page(2,5);
        bookDao.selectPage(page,null);
        System.out.println(page.getCurrent());
        System.out.println(page.getSize());
        System.out.println(page.getTotal());
        System.out.println(page.getPages());
        System.out.println(page.getRecords());

    }

    /**
     * 数据层开发：条件查询功能
     * 使用QueryWrapper对象封装查询条件，推荐使用LambdaQueryWrapper对象，所有查询封装成方法调用
     */
    @Test
    void testGetBy() {
        // select * from tbl_book where name like %boot%
        QueryWrapper<Book> qw=new QueryWrapper<>();
        qw.like("name","boot");
        bookDao.selectList(qw);
    }
    @Test
    void testGetBy2() {
        String name="6";
        LambdaQueryWrapper<Book> lqw=new LambdaQueryWrapper<Book>();
        // if(name!=null) lqw.like(Book::getName,name);
        //name是ture就连，name是flase就不连
        //支持动态拼写查询条件
        lqw.like(name!=null,Book::getName,name);
        bookDao.selectList(lqw);
    }

}
