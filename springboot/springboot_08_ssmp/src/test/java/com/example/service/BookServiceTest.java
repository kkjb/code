package com.example.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.domain.Book;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
//使用mp快速开发
@SpringBootTest
public class BookServiceTest {
    @Autowired
    private IBookService bookService;

    @Test
    void testGetById() {
        System.out.println(bookService.getById(4));
    }

    @Test
    void testSave() {
        Book book = new Book();
        book.setType("测试数据boot");
        book.setName("测试数据boot");
        book.setDescription("测试数据boot");
        bookService.save(book);
    }

    @Test
    void testUpdate() {
        Book book = new Book();
        book.setId(17);
        book.setType("测试数据boot123");
        book.setName("测试数据boot");
        book.setDescription("测试数据boot");
        bookService.updateById(book);
    }

    @Test
    void testDelete() {
        bookService.removeById(18);
    }

    @Test
    void testGetAll() {
        bookService.list();
    }

    // 分页操作需要设定分页对象IPage
    @Test
    void testGetPage() {

        IPage<Book> page = new Page<Book>(2,5);
        bookService.page(page);
        System.out.println(page.getCurrent());
        System.out.println(page.getSize());
        System.out.println(page.getTotal());
        System.out.println(page.getPages());
        System.out.println(page.getRecords());

    }
}
