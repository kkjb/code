package com.example.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.example.domain.Book;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

//废弃了
@SpringBootTest
public class BookServiceTestCase {
    @Autowired
    private BookService bookService;

    @Test
    void testGetById() {
        System.out.println(bookService.getById(4));
    }

    @Test
    void testSave() {
        Book book = new Book();
        book.setType("测试数据boot");
        book.setName("测试数据boot");
        book.setDescription("测试数据boot");
        bookService.save(book);
    }

    @Test
    void testUpdate() {
        Book book = new Book();
        book.setId(16);
        book.setType("测试数据bootnew");
        book.setName("测试数据boot");
        book.setDescription("测试数据boot");
        bookService.update(book);
    }

    @Test
    void testDelete() {
        bookService.delete(10);
    }

    @Test
    void testGetAll() {
        bookService.getAll();
    }

    // 分页操作需要设定分页对象IPage
    @Test
    void testGetPage() {
        IPage<Book> page = bookService.getPage(2, 5);
        System.out.println(page.getCurrent());
        System.out.println(page.getSize());
        System.out.println(page.getTotal());
        System.out.println(page.getPages());
        System.out.println(page.getRecords());

    }
}
