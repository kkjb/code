package com.example.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.domain.Book;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
//继承BaseMapper并指定泛型
public interface BookDao extends BaseMapper<Book> {
    // @Select("select * from tbl_book where id=#{id}")
    // Book getById(Integer id);
}
