package com.example.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.example.controller.untils.R;
import com.example.domain.Book;
import com.example.service.IBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

// 接入业务操作成功或失败返回数据格式
// 实体数据用@RequestBody（json），路径变量用@PathVariable("{id}")
// 表现层接口开发
@RestController
@RequestMapping("/books")
public class BookController {
    @Autowired
    private IBookService bookService;

    @GetMapping
    public R getAll() {
        return new R(true, bookService.list());
    }

    //可以在表现层Controller中进行消息统一处理
    @PostMapping
    public R save(@RequestBody Book book) throws IOException {
        // R r=new R();
        // boolean flag=bookService.save(book);
        // r.setFlag(flag);
        // 手动制作异常，新建图书时，如果图书名称为123则异常
        if (book.getName().equals("123")) throw new IOException();
        boolean flag = bookService.save(book);
        // 使用三目运算，根据结果不同加载不同的消息
        return new R(flag, flag ? "添加成功" : "添加失败");
    }

    @PutMapping
    public R update(@RequestBody Book book) {
        return new R(bookService.modify(book));
    }

    @DeleteMapping("{id}")
    public R delete(@PathVariable Integer id) {
        return new R(bookService.delete(id));
    }

    // http://localhost:8080/books/2
    @GetMapping("{id}")
    public R getById(@PathVariable Integer id) {
        return new R(true, bookService.getById(id));
    }

    //删除功能维护
    //分页查询
    //使用路径参数传递分页数据或封装对象传递数据
    //对查询结果进行校验，如果当前页码值大于最大页码值，使用最大页码值作为当前页码值重新查询
    // @GetMapping("{currentPage}/{pageSize}")
    // public R getPage(@PathVariable int currentPage, @PathVariable int pageSize) {
    //     IPage<Book> page=bookService.getPage(currentPage, pageSize);
    //     //如果页面当前页码值大于了总页码值，那么重新执行查询操作，使用最大页码值作为当前页码值
    //     if(currentPage>page.getPages()){
    //         page=bookService.getPage((int)page.getPages(), pageSize);
    //     }
    //     return new R(true, page);
    // }

    //Controller接收参数
    //Controller调用业务层分页条件查询接口
    @GetMapping("{currentPage}/{pageSize}")
    public R getPage(@PathVariable int currentPage, @PathVariable int pageSize,Book book) {
        // System.out.println("参数==>"+book);
        IPage<Book> page=bookService.getPage(currentPage, pageSize,book);
        //如果页面当前页码值大于了总页码值，那么重新执行查询操作，使用最大页码值作为当前页码值
        if(currentPage>page.getPages()){
            page=bookService.getPage((int)page.getPages(), pageSize,book);
        }
        return new R(true, page);
    }
}
