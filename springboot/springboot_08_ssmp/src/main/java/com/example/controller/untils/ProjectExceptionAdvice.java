package com.example.controller.untils;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

//对异常进行统一处理，出现异常后，返回指定信息
//作为springmvc的异常处理器
//定义它是controller的异常处理器（@ControllerAdvice或@RestControllerAdvice）
@RestControllerAdvice
//使用注解@RestControllerAdvice定义springmvc异常处理器用来处理异常
//异常处理器必须被扫描加载，否则无法生效
public class ProjectExceptionAdvice {
    //拦截所有的异常信息
    @ExceptionHandler(Exception.class)
    public R doException(Exception ex){
        //记录日志
        //发送消息给运维
        //发送邮件给开发人员，ex对象发送给开发人员
        ex.printStackTrace();
        return new  R("服务器故障，请稍后再试!");
    }
}
