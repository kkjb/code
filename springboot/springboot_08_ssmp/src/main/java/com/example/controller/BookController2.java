package com.example.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.example.domain.Book;
import com.example.service.IBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//废弃了
//实体数据用@RequestBody（json），路径变量用@PathVariable("{id}")
//表现层接口开发
// @RestController
@RequestMapping("/books")
public class BookController2 {
    @Autowired
    private IBookService bookService;
    @GetMapping
    public List<Book> getAll(){
        return bookService.list();
    }
    @PostMapping
    public boolean save(@RequestBody Book book){
        return bookService.save(book);
    }
    @PutMapping
    public boolean update(@RequestBody Book book){
        return bookService.modify(book);
    }
    @DeleteMapping("{id}")
    public boolean delete(@PathVariable Integer id){
        return bookService.delete(id);
    }
    // http://localhost:8080/books/2
    @GetMapping("{id}")
    public Book getById(@PathVariable Integer id){
        return bookService.getById(id);
    }
    @GetMapping("{currentPage}/{pageSize}")
    public IPage<Book> getPage(@PathVariable int currentPage,@PathVariable int pageSize){
        return bookService.getPage(currentPage, pageSize, null);
    }
}
