package com.example.domain;

import lombok.Data;

//lombok
@Data
//为当前的实体类在编译器设置对应的get/set方法，toString方法，hashCode方法，equals方法
public class Book {
    private Integer id;
    private String type;
    private String name;
    private String description;
}
