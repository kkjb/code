package com.example.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.domain.Book;
import org.apache.ibatis.annotations.Mapper;

//使用MyBatisPlus
@Mapper
//继承BaseMapper并指定泛型
public interface BookDao extends BaseMapper<Book> {
}
