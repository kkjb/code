package com.example.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.domain.Book;

/**
 * 业务层开发：快速开发
 * 快速开发方案
 * 1.使用MyBatisPlus提供有业务通用接口（IService<T>）与业务层通用实现类（ServiceImpl<M,T>）
 * 2.在通用类基础上做功能的重载或功能追加
 * 3.注意重载时不要覆盖原始操作，避免原始提供的功能消失
 */
//接口定义（快速开发）
public interface IBookService extends IService<Book> {
    //写个方法，如果加上@Overide出现报错，则表示重名，需要换个名称
    //追加的操作与原始操作通过名称区分，功能类似
    boolean saveBook(Book book);
    boolean modify(Book book);
    boolean delete(Integer id);
    IPage<Book> getPage(int currentPage,int pageSize);
    IPage<Book> getPage(int currentPage,int pageSize,Book book);
}
