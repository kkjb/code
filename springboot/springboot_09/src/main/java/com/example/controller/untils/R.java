package com.example.controller.untils;

import lombok.Data;

//使用lombok
//修改表现层返回结果的模型类，封装出现异常后对应的信息
@Data
public class R {
    private boolean flag;
    private Object data;
    private String msg;//要显示的信息
    //构造方法根据实际使用来调，用多少加多少

    public R() {
    }

    public R(boolean flag) {
        this.flag = flag;
    }

    public R(boolean flag, Object data) {
        this.flag = flag;
        this.data = data;
    }

    public R(boolean flag, String msg) {
        this.flag = flag;
        this.msg = msg;
    }

    public R(String msg) {
        this.msg = msg;
    }
}
