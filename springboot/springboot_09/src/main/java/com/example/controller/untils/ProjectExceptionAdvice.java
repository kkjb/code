package com.example.controller.untils;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestControllerAdvice;

//对异常进行统一处理，出现异常后，返回指定信息
//作为springmvc的异常处理器
//定义它是controller的异常处理器（@ControllerAdvice或@RestControllerAdvice）
@RestControllerAdvice
//使用注解@RestControllerAdvice定义springmvc异常处理器来处理异常
public class ProjectExceptionAdvice {
    //拦截所有异常信息
    @ExceptionHandler(Exception.class)
    public R doException(Exception ex){
        //记录日志
        //发送消息给运维
        //发邮件给开发人员，ex对象发送给开发人员
        ex.printStackTrace();//打印异常信息在控制台上
        return new R("服务器故障，请稍后再试！");
    }
}
