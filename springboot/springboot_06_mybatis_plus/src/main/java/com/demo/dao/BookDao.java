package com.demo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.demo.domain.Book;
import org.apache.ibatis.annotations.Mapper;

//2.定义数据层接口与映射配置，继承BaseMapper
//3.其他同SpringBoot整合MyBatis
@Mapper
public interface BookDao extends BaseMapper<Book> {
}
