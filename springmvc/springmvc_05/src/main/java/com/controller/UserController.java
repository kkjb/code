package com.controller;

import com.domain.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * RESTful入门案例
 * 1.设定http请求动作（动词）method = RequestMethod.POST（POST增加，DELETE删除，PUT修改，GET查询）
 * 2.设定请求参数（路径变量）（/users/{id}，@PathVariable Integer id）
 */

/**
 * @RequestBody，@RequestParam，@PathVariable
 * 区别：
 * 1.@RequestParam用于接收url地址传参或表单传参
 * 2.@RequestBody用于接收json数据
 * 3.@PathVariable用于接收路径参数，使用{参数名称}描述路径参数
 * 应用：
 * 1.后期开发钟，发送请求参数超过1小时，以json为主，@RequestBody应用较广
 * 2.如果发送非json格式数据，选用@RequestParam接收请求参数
 * 3.采用RESTful进行开发，挡参数数量较少时，例如1个，可以采用@PathVariable接收请求路径变量，通常用于传递id值
 */
@Controller
public class UserController {
    @RequestMapping(value = "/users", method = RequestMethod.POST)
    /**
     * @RequestMapping("")
     * 类型：方法注解
     * 位置：SpringMvc控制器方法定义上方
     * 作用：设置当前控制器方法请求访问路径
     * 属性：
     * value（默认）：请求访问路径，或访问路径前缀
     * method：http请求动作，标准动作（GET/POST/PUT/DELETE）
     */
    @ResponseBody
    public String save(@RequestBody User user) {
        System.out.println("user save..." + user);
        return "{'module':'user save'}";
    }

    @RequestMapping(value = "/users/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public String delete(@PathVariable Integer id) {
        /**
         * @PathVariable
         * 类型：形参注解
         * 位置：SpringMVC控制器方法形参定义前面
         * 作用：绑定路径参数与处理器方法形参间的关系，要求路径参数名与形参名一一对应
         */
        System.out.println("user delete..." + id);
        return "{'module':'user delete'}";
    }

    @RequestMapping(value = "/users", method = RequestMethod.PUT)
    @ResponseBody
    public String update(@RequestBody User user) {
        System.out.println("user update..." + user);
        return "{'module':'user update'}";
    }

    @RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
    @ResponseBody
    public String getById(@PathVariable Integer id) {
        System.out.println("user getById..." + id);
        return "{'module':'user getById'}";
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    @ResponseBody
    public String getAll() {
        System.out.println("user getAll...");
        return "{'module':'user getAll'}";
    }
}
