package com.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@ComponentScan("com.controller")
@EnableWebMvc
/**
 * @EnableWebMvc
 * 类型：配置类注解
 * 位置：SpringMVC配置类定义上方
 * 作用：开启SpringMVC多项辅助功能
 */
public class SpringMvcConfig {
}
