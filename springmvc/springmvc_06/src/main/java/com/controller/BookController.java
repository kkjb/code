package com.controller;

import com.domain.Book;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * 基于RESTful页面数据交互
 * 1.制作SpringMVC控制器，并通过PostMan测试接口功能（BookController.java）
 * 2.设置静态资源的访问放行（SpringMvcSupport.java）
 * 3.前端页面通过异步提交访问后台控制器（books.html）
 */
@RestController
@RequestMapping("/books")
public class BookController {
    @PostMapping
    public String save(@RequestBody Book book) {
        System.out.println("book save ==>" + book);
        return "{'module':'book save success'}";
    }

    @GetMapping
    public List<Book> getAll() {
        System.out.println("books getAll is running...");
        List<Book> bookList = new ArrayList<Book>();

        Book book1 = new Book();
        book1.setType("计算机");
        book1.setName("SpringMVC入门");
        book1.setDescription("入门");
        bookList.add(book1);

        Book book2 = new Book();
        book2.setType("计算机");
        book2.setName("SpringMVC实战");
        book2.setDescription("牛逼");
        bookList.add(book2);

        Book book3 = new Book();
        book3.setType("计算机操作");
        book3.setName("SpringMVC实战23");
        book3.setDescription("牛逼666");
        bookList.add(book3);

        return bookList;
    }
}
