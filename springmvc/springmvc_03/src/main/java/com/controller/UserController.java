package com.controller;

import com.domain.User;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Get请求参数
 * 普通参数：url地址传参，地址参数名与形参变量名相同，定义形参即可接收参数
 * Post请求参数
 * 普通参数：form表单post请求传参，表单参数名与形参变量名相同，定义形参即可接收参数
 */
@Controller
public class UserController {
    // 普通参数:url地址传参，地址参数名与形参变量名相同，定义形参即可接收参数woshi
    @RequestMapping("/commonParam")
    @ResponseBody
    public String commonParam(String name, int age) {
        System.out.println("普通参数传递 name ==>" + name);
        System.out.println("普通参数传递 age ==>" + age);
        return "{'module':'common param'}";
    }

    // 普通参数：请求参数名和形参名不同，使用@RequestParam绑定参数关系
    @RequestMapping("/commonParamDifferentName")
    @ResponseBody
    public String commonParamDifferentName(@RequestParam("name") String username, int age) {
        /**
         * @RequestParam
         * 类型：形参注解
         * 位置：SpringMVC控制器方法形参定义前面
         * 作用：绑定请求参数与处理器方法形参间的关系
         * 参数：
         * required：是否为必传参数
         * defaultValue：参数默认值
         */
        System.out.println("普通参数传递 username ==>" + username);
        System.out.println("普通参数传递 age ==>" + age);
        return "{'module':'common param different name'}";
    }

    // POJO参数：请求参数名与形参属性名相同，定义POJO类型形参即可接收参数
    @RequestMapping("/pojoParam")
    @ResponseBody
    public String pojoParam(User user) {
        System.out.println("pojo参数传递 user ==>" + user);
        return "{'module':'pojo param'}";
    }

    // 嵌套POJO参数：POJO对象中包含POJO对象，请求参数名与形参对象属性名相同，按照对象层次结构关系即可接收嵌套POJO属性参数
    @RequestMapping("/pojoContainPojoParam")
    @ResponseBody
    public String pojoContainPojoParam(User user) {
        System.out.println("pojo嵌套pojo参数传递 user ==>" + user);
        return "{'module':'pojo contain pojo param'}";
    }

    // 数组参数：请求参数名与形参对象属性名相同且请求参数为多个，定义数组参数类型形参即可接收参数
    @RequestMapping("/arrayParam")
    @ResponseBody
    public String arrayParam(String[] likes) {
        System.out.println("数组参数传递 likes ==>" + Arrays.toString(likes));
        return "{'module':'array param'}";
    }

    // 集合保存普通参数：请求参数名与形参集合对象名相同且请求参数为多个，@RequestParam绑定参数关系
    @RequestMapping("/listParam")
    @ResponseBody
    public String listParam(@RequestParam List<String> likes) {
        System.out.println("集合参数传递 likes ==>" + likes);
        return "{'module':'list param'}";
    }

    /**
     * 请求与响应：接收请求中的json数据
     * 1.添加json数据转换相关坐标
     * 2.设置发送json数据（请求body中添加json数据）在postman中点击Body==>raw==>JSON
     * 3.开启自动转换json数据的支持（在SpringMvcConfig中使用@EnableWebMvc）
     * 4.设置接收json数据（使用@RequestBody）
     */
    // 集合参数：json格式
    @RequestMapping("/listParamForJson")
    @ResponseBody
    public String listParamForJson(@RequestBody List<String> likes) {
        /**
         * @RequestBody
         * 类型：形参注解
         * 位置：SpringMVC控制器方法形参定义前面
         * 作用：将请求中请求体所包含的数据传递给请求参数，此注解一个处理器方法只能使用一次
         */
        /**
         * @RequestParam与@RequestBody区别
         * 区别：
         * @RequestParam用于接收url地址传参，表单传参【value=application/x-www-form-urlencoded】
         * @RequestBody用于接收json数据【value=application/json】
         * 应用：
         * 后期开发中，发送json格式数据为主，@RequestBody应用较广
         * 如果发送非json格式数据，选用@RequestParam接收请求参数
         */
        System.out.println("list common(json)参数传递 list ==>" + likes);
        return "{'module':'list common for json param'}";
    }

    // 请求参数（传递json对象）
    // POJO参数：json格式
    // POJO参数：json数据与形参对象属性名相同，定义POJO类型形参即可接收参数
    @RequestMapping("/pojoParamForJson")
    @ResponseBody
    public String pojoParamForJson(@RequestBody com.domain.User user) {
        System.out.println("pojo(json)参数传递 user ==>" + user);
        return "{'module':'pojo for json param'}";
    }

    // 请求参数（传递json数组）
    // 集合参数：json格式
    // POJO集合参数：json数组数据与集合泛型属性名相同，定义List类型形参即可接收参数
    @RequestMapping("/listPojoParamForJson")
    @ResponseBody
    public String listPojoParamForJson(@RequestBody List<User> list) {
        System.out.println("pojo(json)参数传递 user ==>" + list);
        return "{'module':'list pojo for json param'}";
    }

    // 日期参数
    @RequestMapping("/dataParam")
    @ResponseBody
    public String dataParam(Date date,
                            @DateTimeFormat(pattern = "yyyy-MM-dd") Date date1,
                            @DateTimeFormat(pattern = "yyyy/MM/dd HH:mm:ss") Date date2) {
        /**
         *@DateTimeFormat
         * 类型：形参注解
         * 位置：SpringMVC控制器方法形参前面
         * 作用：设定日期时间型数据格式
         * 属性：pattern：日期时间格式字符串
         */
        System.out.println("参数传递 date ==>" + date);
        System.out.println("参数传递 date1(yyyy-MM-dd) ==>" + date1);
        System.out.println("参数传递 date2(yyyy/MM/dd HH:mm:ss) ==>" + date2);
        return "{'module':'data param'}";
    }
}
