package com.controller;

import com.exception.BusinessException;
import com.exception.SystemException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
//异常处理器（集中的、统一的处理项目中出现的异常）（拦截并处理异常）
//声明这个类是用来做异常处理的，必须得被SpringMvcConfig.java的@ComponentScan("com.controller")加载到
@RestControllerAdvice
/**
 * @RestControllerAdvice
 * 类型：类注解
 * 位置：Rest风格开发的控制器增强类定义上方
 * 作用：为Rest风格开发的控制器类做增强
 * 说明：此注解自带@ResponseBody注解与@Component注解，具备对应的功能
 */
public class ProductExceptionAdvice {
    @ExceptionHandler(SystemException.class)
    public Result doSystemException(SystemException ex){
        //记录日志
        //发生消息给运维
        //发送邮件给开发人员,ex对象发送给开发人员
        return new Result(ex.getCode(),null,ex.getMessage());
    }
    @ExceptionHandler(BusinessException.class)
    public Result doBusinessException(BusinessException ex){
        return new Result(ex.getCode(),null,ex.getMessage());
    }
    //异常处理标志，告诉用来处理什么异常，异常的种类
    @ExceptionHandler(Exception.class)
    /**
     * @ExceptionHandler
     * 类型：方法注解
     * 位置：专用于异常处理的控制器方法上方
     * 作用：设置指定异常的处理方案，功能等同于控制器方法，出现异常后终止原始控制器执行，并转入当前方法执行
     * 说明：此类方法可以根据处理的异常不同，制作多个方法分别处理对应的异常
     */
    //写一个方法，用来处理异常
    public Result doException(Exception ex){
        //记录日志
        //发生消息给运维
        //发送邮件给开发人员,ex对象发送给开发人员
        return new Result(Code.SYSTEM_UNKNOW_ERR,null,"系统繁忙，请稍后重试，出现其他未知异常");
    }
}
