package com.exception;
//自定义项目系统级异常
//继承运行时异常
public class SystemException extends RuntimeException{
    //自定义编号，方便识别是那种异常
    private Integer code;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public SystemException(Integer code, String message) {
        super(message);
        this.code = code;
    }

    public SystemException(Integer code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }

}
