package com.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.transaction.annotation.EnableTransactionManagement;

//设置它是个配置类
@Configuration
//加载它控制的bean，直接小范围，反正误扫描
@ComponentScan({"com.service"})
//加载properties类
@PropertySource("classpath:jdbc.properties")
//加载配置类
@Import({JdbcConfig.class, MyBatisConfig.class})
//开启注解式事务驱动
@EnableTransactionManagement
public class SpringConfig {
}
