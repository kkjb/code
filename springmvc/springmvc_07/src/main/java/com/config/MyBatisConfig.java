package com.config;

import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.mapper.MapperScannerConfigurer;
import org.springframework.context.annotation.Bean;

import javax.sql.DataSource;

public class MyBatisConfig {
    //造整合的SqlSessionFactoryBean
    @Bean
    public SqlSessionFactoryBean sqlSessionFactory(DataSource dataSource){
        //造对象
        SqlSessionFactoryBean factoryBean=new SqlSessionFactoryBean();
        //提供DataSource
        factoryBean.setDataSource(dataSource);
        //类型别名的扫描包
        factoryBean.setTypeAliasesPackage("com.domain");
        return factoryBean;
    }
    //扫描映射
    @Bean
    public MapperScannerConfigurer mapperScannerConfigurer(){
        //造对象
        MapperScannerConfigurer msc=new MapperScannerConfigurer();
        //指定映射从哪加载（接口的形式）
        msc.setBasePackage("com.dao");
        return msc;
    }
}
