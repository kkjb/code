package com.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

//web容器的配置类
public class ServletConfig extends AbstractAnnotationConfigDispatcherServletInitializer {
    protected Class<?>[] getRootConfigClasses() {
        //加载spring的核心配置，根配置
        return new Class[]{SpringConfig.class};
    }

    protected Class<?>[] getServletConfigClasses() {
        //加载springmvc的核心配置
        return new Class[]{SpringMvcConfig.class};
    }

    protected String[] getServletMappings() {
        //拦截所有的请求
        return new String[]{"/"};
    }
}
