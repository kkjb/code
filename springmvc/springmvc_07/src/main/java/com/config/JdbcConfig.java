package com.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;

public class JdbcConfig {
    // 使用加载信息
    @Value("${jdbc.driver}")
    // 构造成员变量
    private String driver;
    @Value("${jdbc.url}")
    private String url;
    @Value("${jdbc.username}")
    private String username;
    @Value("${jdbc.password}")
    private String password;

    // 建造Druid数据源
    @Bean
    public DataSource dataSource() {
        DruidDataSource dataSource = new DruidDataSource();
        // 注入
        dataSource.setDriverClassName(driver);
        dataSource.setUrl(url);
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        return dataSource;
    }

    @Bean
    // 平台事务管理器
    public PlatformTransactionManager transactionManager(DataSource dataSource) {
        // 具体的事务管理器实现类
        DataSourceTransactionManager ds = new DataSourceTransactionManager();
        // 挂上dataSource数据源
        ds.setDataSource(dataSource);
        return ds;
    }
}
