package com.service.impl;

import com.controller.Code;
import com.dao.BookDao;
import com.domain.Book;
import com.exception.BusinessException;
import com.exception.SystemException;
import com.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

//实现类，实现BookService
@Service
public class BookServiceImpl implements BookService {
    //自动装配
    @Autowired
    //注入dao接口,爆红不影响
    private BookDao bookDao;
    //实现类
    public boolean save(Book book) {
        //让数据可以传递错误的情况
        return bookDao.save(book)>0;
    }

    public boolean update(Book book) {
        return bookDao.update(book)>0;
    }

    public boolean delete(Integer id) {
        return bookDao.delete(id)>0;
    }

    public Book getById(Integer id) {
        if(id==1){
            throw new BusinessException(Code.BUSINESS_ERR,"请不要乱输入，出现业务异常，id=1");
        }
        // //将可能出现的异常进行包装，转换成自定义异常
        // try {
        //     int i=1/0;
        // }catch (ArithmeticException e){
        //     throw new SystemException(Code.SYSTEM_TIMEOUT_ERR,"服务器访问超时，请重试，出现系统异常1/0",e);
        // }

        return bookDao.getById(id);
    }


    public List<Book> getAll() {
        return bookDao.getAll();
    }
}
