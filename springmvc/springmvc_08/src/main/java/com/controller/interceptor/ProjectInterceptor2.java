package com.controller.interceptor;

import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

// 受spring容器的管理，让环境加载到他
// 自己定义一个他，实现接口HandlerInterceptor，配整个Bean，@Component，然后别忘记了SpringMvcConfig.java中的@ComponentScan({"com.controller","com.config"})扫描
// 声明拦截器的bean，并实现HandlerInterceptor接口（注意：扫描加载bean）
@Component
public class ProjectInterceptor2 implements HandlerInterceptor {
    // 覆盖三个方法
    // 在原始操作被拦截之前运行的代码，一定会执行的
    /**
     * 参数：
     * request：请求对象
     * response：相应对象
     * handler：被调用的处理器对象，本质上是一个方法对象，对反射技术中的Method对象进行了再包装
     * 返回值：
     * 返回值为false，被拦截的处理器将不执行
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // String contentType=request.getHeader("Content-Type");//可以拿到postman中Headers中的Content-Type的value
        // HandlerMethod hm=(HandlerMethod)handler;//通过hm可以拿到原始执行的对象
        System.out.println("preHandle...222");
        // 一定要return true，如果false，将终止原始操作的运行
        return true;
    }

    // 在原始操作被拦截之后运行的代码
    //modelAndView：如果处理器执行完成具有返回结果，可以读取到对应数据与页面信息，并进行调整
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        System.out.println("postHandle...222");
    }

    // 在原始操作被拦截之后运行的代码，并且他是在postHandle执行的后面
    //ex：如果处理器执行过程中出现异常对象，可以针对异常情况进行单独处理
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        System.out.println("afterCompletion...222");
    }
}
