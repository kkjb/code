package com.controller;

import com.domain.Book;
import org.springframework.web.bind.annotation.*;

@RestController
/**
 * @RestController
 * 类型：类注解
 * 位置：基于SpringMVC的RESTful开发控制器类定义上方
 * 作用：设置当前控制器类为RESTful风格，等同于@Controller与@ResponseBody两个注解组合功能
 */
@RequestMapping("/books")
public class BookController {
    @PostMapping
    /**
     * @PostMapping，@DeleteMapping，@PutMapping，@GetMapping
     * 类型：方法注解
     * 位置：基于SpringMVC的RESTful开发控制器方法定义上方
     * 作用：设置当前控制器方法请求访问路径与请求动作，每种对应一个请求动作，例如@GetMapping对应GET请求
     * 属性：
     * value（默认）：请求访问路径
     */
    public String save(@RequestBody Book book) {
        System.out.println("book save..." + book);
        return "{'module':'book save'}";
    }

    @DeleteMapping("/{id}")
    public String delete(@PathVariable Integer id) {
        System.out.println("book delete..." + id);
        return "{'module':'book delete'}";
    }

    @PutMapping
    public String update(@RequestBody Book book) {
        System.out.println("book update..." + book);
        return "{'module':'book update'}";
    }

    @GetMapping("/{id}")
    public String getById(@PathVariable Integer id) {
        System.out.println("book getById..." + id);
        return "{'module':'book getById'}";
    }

    @GetMapping
    public String getAll() {
        System.out.println("book getAll...");
        return "{'module':'book getAll'}";
    }
}
