package com.config;

import com.controller.interceptor.ProjectInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

// 写配置类，在配置类里继承WebMvcConfigurationSupport，然后去覆盖addInterceptors方法，如果有用到Bean就使用@Autowired自动装配，配置projectInterceptor和需要拦截的东西books
// 定义一个配置类，继承WebMvcConfigurationSupport，并实现addInterceptors方法（注意：扫描加载配置）
@Configuration
public class SpringMvcSupport extends WebMvcConfigurationSupport {
    // 拦截器在容器里，给他注入进去
    @Autowired
    private ProjectInterceptor projectInterceptor;

    // 过滤器的配置
    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        // 当访问/page/？？？？时候，走/pages目录下的内容
        registry.addResourceHandler("/pages/**").addResourceLocations("/pages/");
    }

    // 拦截器的配置
    // 指定好通知类addInterceptor(projectInterceptor)，指定路径，访问哪个东西的时候要拦截，addPathPatterns("/books")当我调用books这个请求的时候拦截
    // 添加拦截器并设定访问路径，路径可以通过可变参数设置多个
    @Override
    protected void addInterceptors(InterceptorRegistry registry) {
        //"/books/*"所有books开头的都拦截
        registry.addInterceptor(projectInterceptor).addPathPatterns("/books", "/books/*");
    }
}
