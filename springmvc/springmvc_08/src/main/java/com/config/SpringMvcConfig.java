package com.config;

import com.controller.interceptor.ProjectInterceptor;
import com.controller.interceptor.ProjectInterceptor2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
//如果需要扫描SpringMvcSupport.java包需要@ComponentScan({"com.controller","com.config"})
@ComponentScan("com.controller")
@EnableWebMvc
/**
 * @EnableWebMvc
 * 类型：配置类注解
 * 位置：SpringMVC配置类定义上方
 * 作用：开启SpringMVC多项辅助功能
 */
//使用标准接口WebMvcConfigurer简化开发（注意：侵入式较强），这么做代表跟spring的api关联在一起了，相比之前走SpringMvcSupport.java包具有一定的侵入性
/**
 * 拦截器的执行流程
 * 无拦截器：直接执行controller
 * 有拦截器：preHandle->return判断，如果true->controller->postHandle->afterCompletion，如果return判断false，直接结束
 */
/**
 * 多拦截器执行顺序
 * 当配置多个拦截器时，形成拦截器链
 * 拦截器链的运行顺序参照拦截器的添加顺序为准
 * 当拦截器中出现对原始处理器的拦截，后面的拦截器的均终止运行
 * 当拦截器运行中断，仅运行前面的拦截器afterCompletion操作
 * 全return true：pre1->pre2->pre3->controller->post3->post2->post1->after3->after2->after1
 * pre3 return false：pre1->pre2->pre3->after2->after1
 * pre2 return false：pre1->pre2->after1
 * pre1 return false：pre1
 */
public class SpringMvcConfig implements WebMvcConfigurer {
    // 拦截器在容器里，给他注入进去
    @Autowired
    private ProjectInterceptor projectInterceptor;
    @Autowired
    private ProjectInterceptor2 projectInterceptor2;

    // 拦截器的配置
    // 指定好通知类addInterceptor(projectInterceptor)，指定路径，访问哪个东西的时候要拦截，addPathPatterns("/books")当我调用books这个请求的时候拦截
    // 添加拦截器并设定访问路径，路径可以通过可变参数设置多个
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //"/books/*"所有books开头的都拦截
        registry.addInterceptor(projectInterceptor).addPathPatterns("/books", "/books/*");
        registry.addInterceptor(projectInterceptor2).addPathPatterns("/books", "/books/*");
    }
}
