package com.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.stereotype.Controller;

@Configuration
// @ComponentScan({"com.net.service","com.net.dao"})//扫描包,如果不扫dao包通用性会差一些
@ComponentScan(value = "com",// 扫描这里的所有东西
        excludeFilters = @ComponentScan.Filter(// 设定过滤的规则
                type = FilterType.ANNOTATION,// 按注解过滤
                classes = Controller.class// 过滤的注解类型
        )
)
/**
 * @ComponentScan
 * 类型：类注解
 * 属性：
 * excludeFilters：排除扫描路径中加载的bean，需要指定类别（type）与具体项（classes）
 * includeFilters：加载指定的bean，需要指定类别（type）与具体项（classes）
 */
public class SpringConfig {
}
