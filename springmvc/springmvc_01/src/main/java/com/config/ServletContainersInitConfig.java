package com.config;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
import org.springframework.web.servlet.support.AbstractDispatcherServletInitializer;

// bean的加载格式
public class ServletContainersInitConfig extends AbstractAnnotationConfigDispatcherServletInitializer {// 加载容器

    //简化开发
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{SpringConfig.class};
    }

    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{SpringMvcConfig.class};
    }

    protected String[] getServletMappings() {
        return new String[]{"/"};
    }

}
/*//bean的加载格式
public class ServletContainersInitConfig extends AbstractDispatcherServletInitializer {// 加载容器

    //加载springmvc容器
    protected WebApplicationContext createServletApplicationContext() {
        AnnotationConfigWebApplicationContext ctx = new AnnotationConfigWebApplicationContext();// 初始化容器
        ctx.register(SpringMvcConfig.class);// 将配置注册到容器中
        return ctx;
    }

    //加载spring容器
    protected WebApplicationContext createRootApplicationContext() {// 加载除了SpringMVC之外的Spring容器对应的内容
        AnnotationConfigWebApplicationContext ctx = new AnnotationConfigWebApplicationContext();
        ctx.register(SpringConfig.class);//加载配置
        return ctx;
    }

    protected String[] getServletMappings() {// 设置哪些请求归SpringMVC所处理
        return new String[]{"/"};// 使用/表示所有请求
    }
}*/
