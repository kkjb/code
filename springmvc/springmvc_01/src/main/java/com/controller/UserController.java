package com.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
/**
 * 类型：类注解
 * 位置：位于SpringMVC控制器上方
 * 作用：设定SpringMVC的核心控制器bean
  */
public class UserController {
    @RequestMapping("/save")
    /**
     * @RequestMapping
     * 类型：方法注解
     * 位置：SpringMVC控制器定义上方
     * 作用：设置当前控制器方法请求访问路径
     */
    @ResponseBody
    /**
     * @ResponseBody
     * 类型：方法注解
     * 位置：SpringMVC控制器方法定义上方
     * 作用：设置当前控制器返回值作为响应体
     */
    public String save(){
        System.out.println("uer save...");
        return "{'info':'springmvc'}";
    }
}
