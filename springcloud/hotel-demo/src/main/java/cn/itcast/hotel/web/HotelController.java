package cn.itcast.hotel.web;

import cn.itcast.hotel.pojo.PageResult;
import cn.itcast.hotel.pojo.RequestParams;
import cn.itcast.hotel.service.IHotelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author: 五更琉璃
 * @date: 2023/5/27 18:43
 */
@RestController
@RequestMapping("/hotel")
public class HotelController {
	@Autowired
	private IHotelService hotelService;
	//酒店查询
	@PostMapping("/list")
	public PageResult search(@RequestBody RequestParams params) {
	    return hotelService.search(params);
	}
	//聚合查询
	@PostMapping("filters")
	public Map<String, List<String>> getFilters (@RequestBody RequestParams params){
		return hotelService.filters(params);
	}
	//自动补全
	@GetMapping("suggestion")
	public List<String> getSuggestions(@RequestParam("key") String prefix){
		return hotelService.getSuggestions(prefix);
	}
}
