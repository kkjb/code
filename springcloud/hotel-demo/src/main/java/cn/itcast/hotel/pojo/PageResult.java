package cn.itcast.hotel.pojo;

import lombok.Data;

import java.util.List;

/**
 * @author: 五更琉璃
 * @date: 2023/5/27 18:40
 */
//返回结果
@Data
public class PageResult {
	private Long total; //总条数
	private List<HotelDoc> hotels; //酒店数据

	public PageResult() {
	}

	public PageResult(Long total, List<HotelDoc> hotels) {
		this.total = total;
		this.hotels = hotels;
	}

}
