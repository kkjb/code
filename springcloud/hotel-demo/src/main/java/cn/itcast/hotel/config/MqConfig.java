package cn.itcast.hotel.config;

import cn.itcast.hotel.constants.MqConstants;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author: 五更琉璃
 * @date: 2023/6/26 14:26
 */
@Configuration
public class MqConfig {
	//创建交换机的bean
	@Bean
	public TopicExchange topicExchange() {
		//第一个交换机的名称，第二个是交换机的持久化，第三个代表不自动删除交换机
	    return new TopicExchange(MqConstants.HOTEL_EXCHANGE,true,false);
	}
	//创建添加和修改的队列
	@Bean
	public Queue insertQueue(){
		return new Queue(MqConstants.HOTEL_INSERT_QUEUE,true);
	}
	//创建删除的队列
	@Bean
	public Queue deleteQueue(){
		return new Queue(MqConstants.HOTEL_DELETE_QUEUE,true);
	}
	@Bean
	public Binding insertQueueBinding(){
		//指定对谁做绑定，绑定的队列，绑定的交换机，绑定的RoutingKey
		return BindingBuilder.bind(insertQueue()).to(topicExchange()).with(MqConstants.HOTEL_INSERT_KEY);
	}
	@Bean
	public Binding deleteQueueBinding(){
		//指定对谁做绑定，绑定的队列，绑定的交换机，绑定的RoutingKey
		return BindingBuilder.bind(deleteQueue()).to(topicExchange()).with(MqConstants.HOTEL_DELETE_KEY);
	}
}
