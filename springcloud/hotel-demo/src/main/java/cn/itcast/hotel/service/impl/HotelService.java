package cn.itcast.hotel.service.impl;

import cn.itcast.hotel.mapper.HotelMapper;
import cn.itcast.hotel.pojo.Hotel;
import cn.itcast.hotel.pojo.HotelDoc;
import cn.itcast.hotel.pojo.PageResult;
import cn.itcast.hotel.pojo.RequestParams;
import cn.itcast.hotel.service.IHotelService;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.geo.GeoPoint;
import org.elasticsearch.common.unit.DistanceUnit;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.functionscore.FunctionScoreQueryBuilder;
import org.elasticsearch.index.query.functionscore.ScoreFunctionBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.elasticsearch.search.suggest.Suggest;
import org.elasticsearch.search.suggest.SuggestBuilder;
import org.elasticsearch.search.suggest.SuggestBuilders;
import org.elasticsearch.search.suggest.completion.CompletionSuggestion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 五更琉璃
 */
@Service
public class HotelService extends ServiceImpl<HotelMapper, Hotel> implements IHotelService {

	@Autowired
	private RestHighLevelClient client;

	//酒店查询
	@Override
	public PageResult search(RequestParams params) {
		try {
			//1.创建SearchRequest对象
			SearchRequest request = new SearchRequest("hotel");
			//2.准备resource()，也就是DSL，在es的搜索api中，是利用构建器帮我们构建DSL的，不是以前那样拼json了。QueryBuilder是一个接口，它里面有很多种不同的实现，QueryBuilders是一个工具，里面有各种查询
			//RestAPI中其中构建DSL是通过HighLevelRestClient中的resource()来实现的，其中包含了查询、排序、分页高亮等所有功能
			//2.1query关键字搜索
			buildBasicQuery(params, request);
			//2.2分页
			int page = params.getPage();
			int size = params.getSize();
			request.source().from((page - 1) * size).size(size);
			//2.3排序
			String location = params.getLocation();
			if (location != null && !location.equals("")) {
				//指定一下字段和中心点坐标
				request.source().sort(SortBuilders.geoDistanceSort("location", new GeoPoint(location)).order(SortOrder.ASC).unit(DistanceUnit.KILOMETERS));
			}
			//3.发送请求，得到结果
			SearchResponse response = client.search(request, RequestOptions.DEFAULT);
			//4.解析结果（参考JSON结果，由外到内，逐层解析），拿到hits
			//把他作为结果返回
			return handleResponse(response);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	//聚合查询
	@Override
	public Map<String, List<String>> filters(RequestParams params) {
		try {
			//1.准备Request
			SearchRequest request = new SearchRequest("hotel");
			//2.准备DSL，保证聚合查询时候的查询条件和查询酒店时的查询条件一致
			buildBasicQuery(params, request);
			//2.1设置size，不需要文档，只需要聚合的结果就行了
			request.source().size(0);
			//2.2聚合，把request.source().aggregation()抽取出来，代码会简洁点
			buildAggregation(request);
			//3.发送请求
			SearchResponse response = client.search(request, RequestOptions.DEFAULT);
			//4.解析结果
			//准备好返回的Map
			Map<String, List<String>> result=new HashMap<>();
			Aggregations aggregations = response.getAggregations();
			//4.1根据名称获取品牌结果
			List<String> brandList = getAggByName(aggregations,"brandAgg");
			//4.4放入map
			result.put("brand",brandList);
			//4.1根据名称获取品牌结果
			List<String> cityList = getAggByName(aggregations,"cityAgg");
			//4.4放入map
			result.put("city",cityList);
			//4.1根据名称获取品牌结果
			List<String> starList = getAggByName(aggregations,"starAgg");
			//4.4放入map
			result.put("starName",starList);
			return result;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<String> getSuggestions(String prefix) {
		try {
			//1.准备Request
			SearchRequest request = new SearchRequest("hotel");
			//2.准备DSL
			//添加一个补全查询，自动补全的名字叫做suggestions，第二个参数是一个SuggestBuilder，我们可以使用SuggestBuilders的工具，用自动补全的completionSuggestion，补全的字段叫做suggestion，前缀是h，启动跳过重复的，显示大小为10
			request.source().suggest(new SuggestBuilder().addSuggestion("suggestions", SuggestBuilders.completionSuggestion("suggestion").prefix(prefix).skipDuplicates(true).size(10)));
			//3.发起请求
			SearchResponse response = client.search(request, RequestOptions.DEFAULT);
			//4.解析结果
			//json就是response，我们要从中拿到suggest部分
			Suggest suggest = response.getSuggest();
			//4.1根据名称获取补全结果
			CompletionSuggestion suggestion = suggest.getSuggestion("suggestions");
			//4.2获取options并遍历，options才是最终的补全内容
			List<CompletionSuggestion.Entry.Option> options=suggestion.getOptions();
			//4.3因为得到的是一个数组，所以我们要对整个数组进行遍历，得到每一个option
			List<String> list =new ArrayList<>(options.size());
			for (CompletionSuggestion.Entry.Option option: options){
				//4.4获取一个option中的text，也就是补全的词条
				String text=option.getText().toString();
				list.add(text);
			}
			return list;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void deleteById(Long id) {
		//有异常不能抛出，得try-catch起来
		try {
			//1.准备request
			DeleteRequest request = new DeleteRequest("hotel", id.toString());
			//删除文档不需要参数，只需要知道id就行了
			//2.发送请求
			client.delete(request, RequestOptions.DEFAULT);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void insertById(Long id) {
		//有异常不能抛出，得try-catch起来
		try {
			//0.根据id查询酒店数据
			Hotel hotel = getById(id);
			//转换为文档类型，把数据库类型转换成索引库类型
			HotelDoc hotelDoc = new HotelDoc(hotel);
			//1.准备Request对象，指定id
			IndexRequest request = new IndexRequest("hotel").id(hotel.getId().toString());//获取hotel中的id，将他转成字符串格式
			//2.准备json文档，使用JSON.toJSONString()帮我们把对象进行序列化变成json风格
			request.source(JSON.toJSONString(hotelDoc), XContentType.JSON);
			//3.发送请求
			client.index(request, RequestOptions.DEFAULT);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private static List<String> getAggByName(Aggregations aggregations,String aggName) {
		//4.1根据聚合名称获取聚合结果
		Terms brandTerms = aggregations.get(aggName);
		//4.2获取buckets
		List<? extends Terms.Bucket> buckets = brandTerms.getBuckets();
		//4.3遍历buckets
		//准备好存放的数组
		List<String> brandList=new ArrayList<>();
		for (Terms.Bucket bucket : buckets) {
			String key = bucket.getKeyAsString();
			brandList.add(key);
		}
		return brandList;
	}

	private static void buildAggregation(SearchRequest request) {
		request.source().aggregation(AggregationBuilders.terms("brandAgg").field("brand").size(100));
		request.source().aggregation(AggregationBuilders.terms("cityAgg").field("city").size(100));
		request.source().aggregation(AggregationBuilders.terms("starAgg").field("starName").size(100));
	}

	private static void buildBasicQuery(RequestParams params, SearchRequest request) {
		//1.构建BooleanQuery
		BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
		//关键字搜索应该作为boolean中的must部分
		//从前端传来需要查询的关键字，params里面是什么，就是我们需要查询的东西，我们需要从params里面取出参数
		String key = params.getKey();
		//健壮性判断，防止传入空值
		if (key == null || "".equals(key)) {
			boolQuery.must(QueryBuilders.matchAllQuery());
		} else {
			boolQuery.must(QueryBuilders.matchQuery("all", key));
		}
		//条件过滤
		//城市条件精准搜索
		if (params.getCity() != null && !params.getCity().equals("")) {
			//过滤条件不要放在must里面，不然会影响得分
			boolQuery.filter(QueryBuilders.termQuery("city", params.getCity()));
		}
		//品牌条件精准搜索
		if (params.getBrand() != null && !params.getBrand().equals("")) {
			//过滤条件不要放在must里面，不然会影响得分
			boolQuery.filter(QueryBuilders.termQuery("brand", params.getBrand()));
		}
		//星级条件精准搜索
		if (params.getStarName() != null && !params.getStarName().equals("")) {
			//过滤条件不要放在must里面，不然会影响得分
			boolQuery.filter(QueryBuilders.termQuery("starName", params.getStarName()));
		}
		//价格条件，范围过滤
		if (params.getMinPrice() != null && params.getMaxPrice() != null) {
			boolQuery.filter(QueryBuilders.rangeQuery("price").gte(params.getMinPrice()).lte(params.getMaxPrice()));
		}
		//2.算分控制
		FunctionScoreQueryBuilder functionScoreQuery =
				//QueryBuilders.functionScoreQuery是构建functionScore，它分为两部分：boolQuery原始查询，是做相关性算分
				QueryBuilders.functionScoreQuery(
						//第一个原始查询算分，相关性算分的查询
						boolQuery,
						//第二个参数是算分函数的，FunctionScoreQueryBuilder是一个数组，需要new一个，他是个内部类，需要FunctionScoreQueryBuilder.FilterFunctionBuilder
						//function score的数组
						new FunctionScoreQueryBuilder.FilterFunctionBuilder[]{
								//这个数组中要有过滤条件和算分函数
								//其中的一个function score元素
								new FunctionScoreQueryBuilder.FilterFunctionBuilder(
										//过滤条件，满足过滤条件为ture的，才会给他去算分
										QueryBuilders.termQuery("isAD",true),
										//算分函数，权重的算分函数，直接*10
										ScoreFunctionBuilders.weightFactorFunction(10)
								)
						});
		//才是真正的搜索条件
		request.source().query(functionScoreQuery);
	}

	//解析查询结果进行抽取
	private PageResult handleResponse(SearchResponse response) {
		//4.解析结果（参考JSON结果，由外到内，逐层解析），拿到hits
		SearchHits searchHits = response.getHits();
		//4.1查询的总条数，获取total值
		long total = searchHits.getTotalHits().value;
		System.out.println("共搜索到" + total + "条数据");
		//4.2查询结果的数组，获得hits里的hits
		SearchHit[] hits = searchHits.getHits();
		//4.3遍历
		//遍历前准备好打印的数组
		List<HotelDoc> hotels = new ArrayList<>();
		for (SearchHit hit : hits) {
			//获取文档的source
			String json = hit.getSourceAsString();
			//反序列化
			HotelDoc hotelDoc = JSON.parseObject(json, HotelDoc.class);
			//获取排序值
			Object[] sortValues = hit.getSortValues();
			if(sortValues.length>0){
				Object sortValue = sortValues[0];
				hotelDoc.setDistance(sortValue);
			}
			//把每个查询出来的数据添加到数组中去
			hotels.add(hotelDoc);
		}
		//5.封装返回
		return new PageResult(total, hotels);
	}

}
