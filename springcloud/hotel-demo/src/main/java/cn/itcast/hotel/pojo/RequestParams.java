package cn.itcast.hotel.pojo;

import lombok.Data;

/**
 * @author: 五更琉璃
 * @date: 2023/5/27 18:37
 */
//查询
@Data
public class RequestParams {
	private String key; //搜索关键字
	private Integer page; //当前页码
	private Integer size; //每页大小
	private String sortBy; //排序字段
	private String brand; //品牌
	private String starName; //星级
	private String city; //城市
	private Integer minPrice; //最小价格
	private Integer maxPrice; //最大价格
	private String location; //当前位置的地理坐标
}
