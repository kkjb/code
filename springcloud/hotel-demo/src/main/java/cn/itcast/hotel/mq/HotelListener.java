package cn.itcast.hotel.mq;

import cn.itcast.hotel.constants.MqConstants;
import cn.itcast.hotel.service.IHotelService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author: 五更琉璃
 * @date: 2023/6/26 17:10
 */
@Component
public class HotelListener {
	@Autowired
	private IHotelService hotelService;

	/**
	 * 监听酒店新增或修改的业务
	 * @param id 酒店id
	 */
	//指定队列
	@RabbitListener(queues = MqConstants.HOTEL_INSERT_QUEUE)
	//因为发送方发送的是id，所以我们也要用id进行接收
	public void listenerHotelInsertOrUpdate(Long id){
		hotelService.insertById(id);
	}
	/**
	 * 监听酒店删除的业务
	 * @param id 酒店id
	 */
	//指定队列
	@RabbitListener(queues = MqConstants.HOTEL_DELETE_QUEUE)
	//因为发送方发送的是id，所以我们也要用id进行接收
	public void listenerHotelDelete(Long id){
		hotelService.deleteById(id);
	}
}
