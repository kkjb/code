package cn.itcast.hotel.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Data
@NoArgsConstructor
public class HotelDoc {
    private Long id;
    private String name;
    private String address;
    private Integer price;
    private Integer score;
    private String brand;
    private String city;
    private String starName;
    private String business;
    //因为es里面的经度和纬度是合在一起的，所以直接使用location
    private String location;
    private String pic;
    private Object distance;
    private Boolean isAD;
    //completion在java里面是多个词条形成的数组，数组在java里面可以是个集合，所以我们可以使用List，词条是不分词的，所以是字符串，用String就行了
    private List<String> suggestion;//给用户用来做自动补全的

    public HotelDoc(Hotel hotel) {
        this.id = hotel.getId();
        this.name = hotel.getName();
        this.address = hotel.getAddress();
        this.price = hotel.getPrice();
        this.score = hotel.getScore();
        this.brand = hotel.getBrand();
        this.city = hotel.getCity();
        this.starName = hotel.getStarName();
        this.business = hotel.getBusiness();
        this.location = hotel.getLatitude() + ", " + hotel.getLongitude();
        this.pic = hotel.getPic();
        //给suggestion字段进行赋值，由品牌和商圈拼在一起来做的，品牌和商圈形成一个集合放到suggestion里面去了
        //business有可能是包含多个的，我们可以进行切割把他们分开
        if(this.business.contains("、")){
            //business有多个值，需要切割
            String[] arr = this.business.split("、");
            //添加元素
            this.suggestion=new ArrayList<>();
            this.suggestion.add(this.brand);
            //Collections是个工具，我们可以把suggestion给他，然后再把arr给他，他就能帮我们把数组中的元素一个一个添加进去了
            Collections.addAll(this.suggestion,arr);
        } else if (this.business.contains("/")){
                //business有多个值，需要切割
                String[] arr = this.business.split("/");
                //添加元素
                this.suggestion=new ArrayList<>();
                this.suggestion.add(this.brand);
                //Collections是个工具，我们可以把suggestion给他，然后再把arr给他，他就能帮我们把数组中的元素一个一个添加进去了
                Collections.addAll(this.suggestion,arr);
        } else {
            this.suggestion= Arrays.asList(this.brand,this.business);
        }

    }
}
