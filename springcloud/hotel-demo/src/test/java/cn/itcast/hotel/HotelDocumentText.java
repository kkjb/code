package cn.itcast.hotel;

import cn.itcast.hotel.pojo.Hotel;
import cn.itcast.hotel.pojo.HotelDoc;
import cn.itcast.hotel.service.IHotelService;
import com.alibaba.fastjson.JSON;
import org.apache.http.HttpHost;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.List;

@SpringBootTest
public class HotelDocumentText {

	/**
	 * 文档操作的基本步骤：
	 * 初始化RestHighLevelClient
	 * 创建XxxRequest。XXX是Index、Get、Update、Delete
	 * 准备参数（Index和Update时需要）
	 * 发送请求。调用RestHighLevelClient#.xxx()方法，xxx是index、get、update、delete
	 * 解析结果（Get时需要）
	 */
	@Autowired
	private IHotelService hotelService;

	//client的初始化，定义成一个成员变量，定义在成员变量中可以复用
	private RestHighLevelClient client;
	//新增文档
	@Test
	void testAddDocument() throws IOException {
		//根据id查询酒店数据，去数据库里面查询
		Hotel hotel = hotelService.getById(61083L);
		//转换为文档类型，把数据库类型转换成索引库类型
		HotelDoc hotelDoc = new HotelDoc(hotel);
		//1.准备Request对象，指定id
		IndexRequest request = new IndexRequest("hotel").id(hotel.getId().toString());//获取hotel中的id，将他转成字符串格式
		//2.准备json文档，使用JSON.toJSONString()帮我们把对象进行序列化变成json风格
		request.source(JSON.toJSONString(hotelDoc), XContentType.JSON);
		//3.发送请求
		client.index(request, RequestOptions.DEFAULT);
	}

	//查询文档
	@Test
	void testGetDocumentById() throws IOException {
		//1.准备Request
		GetRequest request = new GetRequest("hotel", "61083");//相当于GET /hotel/_doc/61083
		//2.发送请求，得到相应
		GetResponse response = client.get(request, RequestOptions.DEFAULT);//得到整个查询结果
		//3.解析相应结果，把得到的结果转换成json的字符串
		String json = response.getSourceAsString();//提取其中的source然后转换成json的字符串
		//使用fastjson实现反序列化，把json的对象给他，然后告诉他想要的数据类型HotelDoc.class，返回值就是我们想要的对象了
		HotelDoc hotelDoc = JSON.parseObject(json, HotelDoc.class);//返回我们想要的对象
		System.out.println(hotelDoc);
	}

	//文档的局部更新
	@Test
	void testUpdateDocument() throws IOException {
		//1.准备Request
		UpdateRequest request = new UpdateRequest("hotel", "61083");
		//2.准备请求参数
		request.doc(
				"price", "952",
				"starName", "四钻"
		);
		//3.发送请求
		client.update(request, RequestOptions.DEFAULT);
	}

	//删除文档
	@Test
	void testDeleteDocument() throws IOException {
		//1.准备request
		DeleteRequest request = new DeleteRequest("hotel", "61083");
		//删除文档不需要参数，只需要知道id就行了
		//2.发送请求
		client.delete(request, RequestOptions.DEFAULT);
	}

	//批量导入文档
	@Test
	void testBulkRequest() throws IOException {
		//数据库的批量查询，批量查询酒店数据
		List<Hotel> hotels = hotelService.list();
		//1.创建Request
		BulkRequest request = new BulkRequest();
		//2.准备参数，添加多个新增的Request
		for (Hotel hotel : hotels) {
			//Hotel转换成HotelDoc，转换文档类型HotelDoc
			HotelDoc hotelDoc = new HotelDoc(hotel);
			//创建新增文档的request对象
			//request.add(new IndexRequest("hotel").id("61038").source("json",XContentType.JSON));
			request.add(new IndexRequest("hotel")
					.id(hotelDoc.getId().toString())
					.source(JSON.toJSONString(hotelDoc), XContentType.JSON));
		}
		//3.发送请求
		client.bulk(request, RequestOptions.DEFAULT);
	}

	//成员变量的初始化
	@BeforeEach
	void setUp() {
		this.client = new RestHighLevelClient(RestClient.builder(
				//去创建地址，指定ip和端口
				HttpHost.create("http://192.168.186.133:9200")
		));
	}

	//销毁客户端，释放资源
	@AfterEach
	void tearDown() throws IOException {
		this.client.close();
	}

}
