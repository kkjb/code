package cn.itcast.hotel;

import cn.itcast.hotel.pojo.HotelDoc;
import com.alibaba.fastjson.JSON;
import org.apache.http.HttpHost;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.geo.GeoPoint;
import org.elasticsearch.common.unit.DistanceUnit;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.elasticsearch.search.suggest.Suggest;
import org.elasticsearch.search.suggest.SuggestBuilder;
import org.elasticsearch.search.suggest.SuggestBuilders;
import org.elasticsearch.search.suggest.completion.CompletionSuggestion;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@SpringBootTest
public class HotelSearchTest {

	//client的初始化，定义成一个成员变量，定义在成员变量中可以复用
	private RestHighLevelClient client;

	/**
	 * #查询所有
	 * GET /hotel/_search
	 * {
	 * "query": {
	 * "match_all": {}
	 * }
	 * }
	 *
	 * @throws IOException
	 */
	@Test
	void testMatchAll() throws IOException {
		//1.创建SearchRequest对象
		SearchRequest request = new SearchRequest("hotel");
		//2.准备resource()，也就是DSL，在es的搜索api中，是利用构建器帮我们构建DSL的，不是以前那样拼json了。QueryBuilder是一个接口，它里面有很多种不同的实现，QueryBuilders是一个工具，里面有各种查询
		//RestAPI中其中构建DSL是通过HighLevelRestClient中的resource()来实现的，其中包含了查询、排序、分页高亮等所有功能
		request.source().query(QueryBuilders.matchAllQuery());
		//3.发送请求，得到结果
		SearchResponse response = client.search(request, RequestOptions.DEFAULT);
		//4.解析结果（参考JSON结果，由外到内，逐层解析），拿到hits
		handleResponse(response);

	}

	/**
	 * #match查询,根据单个字段查询，"brand","name","business"都在all字段里
	 * #参与查询的字段越多，查询性能越差，最好使用"copy_to"把多个要查的字段放到一个字段中去
	 * GET /hotel/_search
	 * {
	 * "query": {
	 * "match": {
	 * "all":"如家"
	 * }
	 * }
	 * }
	 *
	 * @throws IOException
	 */
	@Test
	void testMatch() throws IOException {
		//1.创建SearchRequest对象
		SearchRequest request = new SearchRequest("hotel");
		//2.准备resource()，也就是DSL，在es的搜索api中，是利用构建器帮我们构建DSL的，不是以前那样拼json了。QueryBuilder是一个接口，它里面有很多种不同的实现，QueryBuilders是一个工具，里面有各种查询
		//RestAPI中其中构建DSL是通过HighLevelRestClient中的resource()来实现的，其中包含了查询、排序、分页高亮等所有功能
		//单字段查询
		request.source().query(QueryBuilders.matchQuery("all", "如家"));
		//多字段查询
		// request.source().query(QueryBuilders.multiMatchQuery("如家","name","business"));
		//3.发送请求，得到结果
		SearchResponse response = client.search(request, RequestOptions.DEFAULT);
		handleResponse(response);

	}

	/**
	 * #term查询，精确匹配，查询不可分割的字段的精确值，搜的值一定要跟文档中的一模一样
	 * GET /hotel/_search
	 * {
	 * "query": {
	 * "term": {
	 * "city": {
	 * "value": "上海"
	 * }
	 * }
	 * }
	 * }
	 *
	 * @throws IOException
	 */
	@Test
	void testTerm() throws IOException {
		//1.创建SearchRequest对象
		SearchRequest request = new SearchRequest("hotel");
		//2.准备resource()，也就是DSL，在es的搜索api中，是利用构建器帮我们构建DSL的，不是以前那样拼json了。QueryBuilder是一个接口，它里面有很多种不同的实现，QueryBuilders是一个工具，里面有各种查询
		//RestAPI中其中构建DSL是通过HighLevelRestClient中的resource()来实现的，其中包含了查询、排序、分页高亮等所有功能
		request.source().query(QueryBuilders.termQuery("city", "上海"));
		//3.发送请求，得到结果
		SearchResponse response = client.search(request, RequestOptions.DEFAULT);
		//4.解析结果（参考JSON结果，由外到内，逐层解析），拿到hits
		handleResponse(response);
	}

	/**
	 * #range查询，范围查询，查询不可分割的字段的值的范围
	 * #gt代表大于，gte代表大于等于，lt代表小于，lte代表小于等于
	 * GET /hotel/_search
	 * {
	 * "query": {
	 * "range": {
	 * "price": {
	 * "gte": 1000,
	 * "lte": 3000
	 * }
	 * }
	 * }
	 * }
	 *
	 * @throws IOException
	 */
	@Test
	void testRange() throws IOException {
		//1.创建SearchRequest对象
		SearchRequest request = new SearchRequest("hotel");
		//2.准备resource()，也就是DSL，在es的搜索api中，是利用构建器帮我们构建DSL的，不是以前那样拼json了。QueryBuilder是一个接口，它里面有很多种不同的实现，QueryBuilders是一个工具，里面有各种查询
		//RestAPI中其中构建DSL是通过HighLevelRestClient中的resource()来实现的，其中包含了查询、排序、分页高亮等所有功能
		request.source().query(QueryBuilders.rangeQuery("price").gte(1000).lte(3000));
		//3.发送请求，得到结果
		SearchResponse response = client.search(request, RequestOptions.DEFAULT);
		//4.解析结果（参考JSON结果，由外到内，逐层解析），拿到hits
		handleResponse(response);
	}

	@Test
	void testBool() throws IOException {
		//1.创建SearchRequest对象
		SearchRequest request = new SearchRequest("hotel");
		//2.准备resource()，也就是DSL，在es的搜索api中，是利用构建器帮我们构建DSL的，不是以前那样拼json了。QueryBuilder是一个接口，它里面有很多种不同的实现，QueryBuilders是一个工具，里面有各种查询
		//RestAPI中其中构建DSL是通过HighLevelRestClient中的resource()来实现的，其中包含了查询、排序、分页高亮等所有功能
		//2.1准备boolQuery
		BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
		//2.2添加term
		boolQuery.must(QueryBuilders.termQuery("city", "上海"));
		boolQuery.should(QueryBuilders.termQuery("brand", "皇家假日"));
		boolQuery.should(QueryBuilders.termQuery("brand", "美年达"));
		boolQuery.mustNot(QueryBuilders.rangeQuery("price").lte(500));
		boolQuery.filter(QueryBuilders.rangeQuery("score").gte("45"));
		request.source().query(boolQuery);
		//3.发送请求，得到结果
		SearchResponse response = client.search(request, RequestOptions.DEFAULT);
		//4.解析结果（参考JSON结果，由外到内，逐层解析），拿到hits
		handleResponse(response);
	}

	/**
	 * #搜索结果处理整体语法
	 * GET /hotel/_search
	 * {
	 *   "query": {
	 *     "match": {
	 *       "name": "如家"
	 *     }
	 *   },
	 *   "from": 0,
	 *   "size": 20,
	 *   "sort": [
	 *     {
	 *       "price": "asc"
	 *     },
	 *     {
	 *       "_geo_distance": {
	 *         "location":"31.040699,121.618075",
	 *         "order": "asc",
	 *         "unit": "km"
	 *       }
	 *     }
	 *   ],
	 *   "highlight": {
	 *     "fields": {
	 *       "name": {
	 *         "pre_tags": "<em>",
	 *         "post_tags": "<em>"
	 *       }
	 *     }
	 *   }
	 * }
	 * 查询，排序，高亮处理
	 * @throws IOException
	 */
	@Test
	void testPageAndSortAndHighlight() throws IOException {
		int page = 1, size = 5;
		//1.创建SearchRequest对象
		SearchRequest request = new SearchRequest("hotel");
		//2.准备resource()，也就是DSL，在es的搜索api中，是利用构建器帮我们构建DSL的，不是以前那样拼json了。QueryBuilder是一个接口，它里面有很多种不同的实现，QueryBuilders是一个工具，里面有各种查询
		//RestAPI中其中构建DSL是通过HighLevelRestClient中的resource()来实现的，其中包含了查询、排序、分页高亮等所有功能
		//2.1query
		request.source().query(QueryBuilders.matchQuery("name", "如家"));
		//2.2排序sort
		request.source().sort("price", SortOrder.ASC);
		request.source().sort(SortBuilders.geoDistanceSort("location", new GeoPoint("31.035082,121.612581")).order(SortOrder.ASC).unit(DistanceUnit.KILOMETERS));
		//2.3分页from、size
		request.source().from((page - 1) * size).size(20);
		//2.4高亮
		request.source().highlighter(new HighlightBuilder()
				.field("name")
				//是否与查询字段匹配
				.requireFieldMatch(false)
				//前置标签
				.preTags("<em>")
				//后置标签
				.postTags("<em>")
		);
		//3.发送请求，得到结果
		SearchResponse response = client.search(request, RequestOptions.DEFAULT);
		//4.解析结果（参考JSON结果，由外到内，逐层解析），拿到hits
		handleResponse(response);
	}

	//聚合查询
	@Test
	void testAggregation() throws IOException {
		//1.准备Request
		SearchRequest request = new SearchRequest("hotel");
		//2.准备DSL
		//2.1设置size，不需要文档，只需要聚合的结果就行了
		request.source().size(0);
		//2.2聚合
		request.source().aggregation(AggregationBuilders.terms("brandAgg").field("brand").size(10));
		//3.发送请求
		SearchResponse response = client.search(request, RequestOptions.DEFAULT);
		//4.解析结果
		Aggregations aggregations = response.getAggregations();
		//4.1根据聚合名称获取聚合结果
		Terms brandTerms = aggregations.get("brandAgg");
		//4.2获取buckets
		List<? extends Terms.Bucket> buckets = brandTerms.getBuckets();
		//4.3遍历buckets
		for (Terms.Bucket bucket : buckets) {
			String key = bucket.getKeyAsString();
			System.out.println(key);
		}
	}

	//解析查询结果进行抽取
	private static void handleResponse(SearchResponse response) {
		//4.解析结果（参考JSON结果，由外到内，逐层解析），拿到hits
		SearchHits searchHits = response.getHits();
		//4.1查询的总条数，获取total值
		long total = searchHits.getTotalHits().value;
		System.out.println("共搜索到" + total + "条数据");
		//4.2查询结果的数组，获得hits里的hits
		SearchHit[] hits = searchHits.getHits();
		//4.3遍历
		for (SearchHit hit : hits) {
			//5.获取文档的source
			String json = hit.getSourceAsString();
			//6.反序列化
			HotelDoc hotelDoc = JSON.parseObject(json, HotelDoc.class);
			//7.高亮结果的处理
			//获取高亮结果，因为highlight的值是个json对象，json对象有key有value，对应java中的Map，这里的key就是高亮字段的名称
			Map<String, HighlightField> highlightFields = hit.getHighlightFields();
			//判断highlight是否为空，不然有可能报空指针错误
			if (!CollectionUtils.isEmpty(highlightFields)) {
				//获取高亮字段结果
				HighlightField highlightField = highlightFields.get("name");
				//判断name是否为空，不然有可能报空指针错误
				if (highlightField != null) {
					//取出高亮结果数组中的第一个，就是酒店名称，highlightField结果，getFragments()拿到数组，因为数组里面只有一个元素，所以直接[0]，string()转成字符串
					String name = highlightField.getFragments()[0].string();
					//set到hotelDoc对象里面，覆盖非高亮结果
					hotelDoc.setName(name);
				}
			}
			System.out.println("hotelDoc=" + hotelDoc);
		}
	}

	@Test
	void testSuggest() throws IOException {
		//1.准备Request
		SearchRequest request = new SearchRequest("hotel");
		//2.准备DSL
		//添加一个补全查询，自动补全的名字叫做suggestions，第二个参数是一个SuggestBuilder，我们可以使用SuggestBuilders的工具，用自动补全的completionSuggestion，补全的字段叫做suggestion，前缀是h，启动跳过重复的，显示大小为10
		request.source().suggest(new SuggestBuilder().addSuggestion("suggestions", SuggestBuilders.completionSuggestion("suggestion").prefix("hz").skipDuplicates(true).size(10)));
		//3.发起请求
		SearchResponse response = client.search(request, RequestOptions.DEFAULT);
		//4.解析结果
		//json就是response，我们要从中拿到suggest部分
		Suggest suggest = response.getSuggest();
		//4.1根据名称获取补全结果
		CompletionSuggestion suggestion = suggest.getSuggestion("suggestions");
		//4.2获取options并遍历，options才是最终的补全内容
		List<CompletionSuggestion.Entry.Option> options=suggestion.getOptions();
		//4.3因为得到的是一个数组，所以我们要对整个数组进行遍历，得到每一个option
		for (CompletionSuggestion.Entry.Option option: options){
			//4.4获取一个option中的text，也就是补全的词条
			String text=option.getText().toString();
			System.out.println(text);
		}
	}

	//成员变量的初始化
	@BeforeEach
	void setUp() {
		this.client = new RestHighLevelClient(RestClient.builder(
				//去创建地址，指定ip和端口
				HttpHost.create("http://192.168.186.133:9200")
		));
	}

	//销毁客户端，释放资源
	@AfterEach
	void tearDown() throws IOException {
		this.client.close();
	}

}
