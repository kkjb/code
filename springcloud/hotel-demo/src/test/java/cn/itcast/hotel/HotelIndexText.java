package cn.itcast.hotel;

import org.apache.http.HttpHost;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.xcontent.XContentType;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static cn.itcast.hotel.constants.HotelConstants.MAPPING_TEMPLATE;

public class HotelIndexText {
	/**
	 * 索引库操作的基本步骤：
	 * 	初始化RestHighLevelClient
	 * 	创建XxxIndexRequest。XXX是Create、Get、Delete
	 * 	准备DSL（ Create时需要）
	 * 	发送请求。调用RestHighLevelClient#indices().xxx()方法，xxx是create、exists、delete
	 */
	//client的初始化，定义成一个成员变量，定义在成员变量中可以复用
	private RestHighLevelClient client;
	//打印client信息
	@Test
	void testInit(){
		System.out.println(client);
	}
	//indices是操作索引库，index是操作文档
	//创建索引库
	@Test
	void testCreateHotelIndex() throws IOException {
		//1.创建Request对象
		CreateIndexRequest request = new CreateIndexRequest("hotel");//相当于PUT /hotel，要跟上索引库名称
		//2.准备请求的参数：DSL。MAPPING_TEMPLATE是静态常量字符串，内容是创建所有库的DSL语句
		request.source(MAPPING_TEMPLATE, XContentType.JSON);
		//3.发送请求，indices就是index的辅助形式，代表了所有的index操作，返回的对象中包含了索引库操作的所有方法
		client.indices().create(request, RequestOptions.DEFAULT);
	}
	//删除索引库
	@Test
	void testDeleteHotelIndex() throws IOException {
		//1.创建Request对象
		DeleteIndexRequest request = new DeleteIndexRequest("hotel");//要跟上索引库名称
		//删除索引库不需要参数，只需要知道名称就行了，所以不需要request.source(MAPPING_TEMPLATE, XContentType.JSON);直接发起请求就行了
		//2.发送请求，indices就是index的辅助形式，代表了所有的index操作，返回的对象中包含了索引库操作的所有方法
		client.indices().delete(request, RequestOptions.DEFAULT);
	}
	//判断索引库是否存在
	@Test
	void testExistsHotelIndex() throws IOException {
		//1.创建Request对象
		GetIndexRequest request = new GetIndexRequest("hotel");//要跟上索引库名称
		//2.发送请求，indices就是index的辅助形式，代表了所有的index操作，返回的对象中包含了索引库操作的所有方法
		boolean exists = client.indices().exists(request, RequestOptions.DEFAULT);
		//3.输出
		System.err.println(exists ? "索引库已经存在！" : "索引库不存在！");
	}
	//成员变量的初始化
	@BeforeEach
	void setUp() {
		this.client = new RestHighLevelClient(RestClient.builder(
				//去创建地址，指定ip和端口
				HttpHost.create("http://192.168.186.133:9200")
		));
	}
	//销毁客户端，释放资源
	@AfterEach
	void tearDown() throws IOException {
		this.client.close();
	}


}
