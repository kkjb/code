package cn.itcast.mq.spring;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringAmqpTest {
	//利用RabbitTemplate的convertAndSend方法发送消息
	@Autowired
	private RabbitTemplate rabbitTemplate;
	@Test
	public void testSendMessage2SimpleQueue(){
		String queueName ="simple.queue";
		String message = "hello,spring amqp!";
		rabbitTemplate.convertAndSend(queueName,message);
	}

	@Test
	public void testSendMessage2WorkQueue() throws InterruptedException {
		String queueName ="simple.queue";
		String message = "hello,message__";
		//连续发送50条消息
		for(int i=1;i<=50;i++){
			rabbitTemplate.convertAndSend(queueName,message+i);
			//1s中发送完成
			Thread.sleep(20);
		}
	}
	@Test
	public void testSendFanoutExchange(){
		//交换机名称
		String exchangeName="root.fanout";
		//消息
		String message = "hello,everyone!";
		//发送消息，参数分别是：交换机名称、RoutingKey（暂时为空）、消息
		rabbitTemplate.convertAndSend(exchangeName,"",message);
	}
	@Test
	public void testSendDirectExchange(){
		//交换机名称
		String exchangeName="root.direct";
		//消息
		String message = "hello,red!";
		//发送消息，参数分别是：交换机名称、RoutingKey（暂时为空）、消息
		rabbitTemplate.convertAndSend(exchangeName,"red",message);
	}
	@Test
	public void testSendTopicExchange(){
		//交换机名称
		String exchangeName="root.topic";
		//消息
		String message = "111";
		//发送消息，参数分别是：交换机名称、RoutingKey（暂时为空）、消息
		rabbitTemplate.convertAndSend(exchangeName,"china.news",message);
	}
	//发送json
	@Test
	public void testSendObjectQueue(){
		Map<String,Object> msg=new HashMap<>();
		msg.put("name","卢本伟");
		msg.put("age",20);
		rabbitTemplate.convertAndSend("object.queue", msg);
	}
}
