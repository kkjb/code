package cn.itcast.mq.listener;

import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.time.LocalTime;
import java.util.Map;

//定义类，添加@Component注解
@Component
public class SpringRabbitListener {

	//类中声明方法，添加@RabbitListener注解，方法参数就时消息
	//注意：消息一旦消费就会从队列删除，RabbitMQ没有消息回溯功能
	// @RabbitListener(queues = "simple.queue")
	// public void ListenerSimpleQueue(String msg) {
	// 	System.out.println("消费者接收到simple.queue的消息：【" + msg + "】");
	// }
	//simple多个消费者之间竞争使用
	@RabbitListener(queues = "simple.queue")
	public void ListenerWorkQueue1(String msg) throws InterruptedException {
		System.out.println("消费者1接收到simple.queue的消息：【" + msg + "】"+ LocalTime.now());
		Thread.sleep(20);
	}
	@RabbitListener(queues = "simple.queue")
	public void ListenerWorkQueue2(String msg) throws InterruptedException {
		System.err.println("消费者2........接收到simple.queue的消息：【" + msg + "】"+ LocalTime.now());
		Thread.sleep(200);
	}
	//fanout广播，每个消费者都能接收到相同的消息
	@RabbitListener(queues = "fanout.queue1")
	public void ListenerFanoutQueue1(String msg) {
		System.out.println("消费者接收到fanout.queue1的消息：【" + msg + "】");
	}
	@RabbitListener(queues = "fanout.queue2")
	public void ListenerFanoutQueue2(String msg) {
		System.out.println("消费者接收到fanout.queue2的消息：【" + msg + "】");
	}
	//direct订阅，只有key值相同的消费者才能收到消息
	@RabbitListener(bindings = @QueueBinding(value = @Queue(name = "direct.queue1"),exchange = @Exchange(name = "root.direct",type = ExchangeTypes.DIRECT),key = {"red","blue"}))
	public void ListenerDirectQueue1(String msg) {
		System.out.println("消费者接收到direct.queue1的消息：【" + msg + "】");
	}
	@RabbitListener(bindings = @QueueBinding(value = @Queue(name = "direct.queue2"),exchange = @Exchange(name = "root.direct",type = ExchangeTypes.DIRECT),key = {"red","yellow"}))
	public void ListenerDirectQueue2(String msg) {
		System.out.println("消费者接收到direct.queue2的消息：【" + msg + "】");
	}
	//TopicExchange发布订阅
	/**
	 * queue与exchange指定bingingkey时可以使用通配符
	 * #：代指0个或者多个单词
	 * *：代指一个单词
	 * 使用场景：
	 * china.new
	 * china.weather
	 * japan.new
	 * japan.weather
	 * 当需要使用全部国家的新闻时或中国的各种信息，就可以使用TopicExchange
	 */
	@RabbitListener(bindings = @QueueBinding(value = @Queue(name = "topic.queue1"),exchange = @Exchange(name = "root.topic",type = ExchangeTypes.TOPIC),key = "china.#"))
	public void ListenerTopicQueue1(String msg) {
		System.out.println("消费者接收到topic.queue1的消息：【" + msg + "】");
	}
	@RabbitListener(bindings = @QueueBinding(value = @Queue(name = "topic.queue2"),exchange = @Exchange(name = "root.topic",type = ExchangeTypes.TOPIC),key = "#.news"))
	public void ListenerTopicQueue2(String msg) {
		System.out.println("消费者接收到topic.queue2的消息：【" + msg + "】");
	}
	//监听object.queue队列并消费信息
	@RabbitListener(queues = "object.queue")
	public void ListenerObjectQueue2(Map<String,Object> msg) {
		System.out.println("消费者接收到object.queue的消息：" + msg );
	}
}
