package cn.itcast.mq.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

//配置类，可以声明各种各样的bean
@Configuration
public class FanoutConfig {
	//声明FanoutExchange交换机
	@Bean
	public FanoutExchange fanoutExchange(){
		return new FanoutExchange("root.fanout");
	}
	//声明第1个队列
	@Bean
	public Queue fanoutQueue1() {
	    return new Queue("fanout.queue1");
	}
	//绑定队列1到交换机
	@Bean
	public Binding fanoutBinding1(Queue fanoutQueue1,FanoutExchange fanoutExchange) {
	    return BindingBuilder.bind(fanoutQueue1).to(fanoutExchange);
	}
	//声明第2个队列
	@Bean
	public Queue fanoutQueue2() {
		return new Queue("fanout.queue2");
	}
	//绑定队列2到交换机
	@Bean
	public Binding fanoutBinding2(Queue fanoutQueue2,FanoutExchange fanoutExchange) {
		return BindingBuilder.bind(fanoutQueue2).to(fanoutExchange);
	}
	//接收json信息
	@Bean
	public Queue objectQueue(){
		return new Queue("object.queue");
	}
}
