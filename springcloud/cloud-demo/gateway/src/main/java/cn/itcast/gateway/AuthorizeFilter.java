package cn.itcast.gateway;

import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;
//请求路由后，会将当前路由过滤器和DefaultFilters、GlobalFilter，合并到一个过滤器链（集合）中，排序后依次执行每个过滤器
//当过滤器的order值一样时，会按照默认过滤器DefaultFilters>路由过滤器>全局过滤器GlobalFilter顺序执行
@Component
// @Order(-1)
/**
 * @Order()是个顺序注解，值越小，优先级越高，过滤器优先执行
 */
public class AuthorizeFilter implements GlobalFilter, Ordered {

	/**
	 * 处理当前请求，有必要的话通过{@link GatewayFilterChain}将请求交给下一个过滤器处理
	 * @param exchange 请求上下文，里面可以获取Request、Response等信息
	 * @param chain 用来把请求委托给下一个过滤器
	 * @return {@code Mono<void>} 返回标示当前过滤器业务结束
	 */

	@Override
	public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
		//1.获取请求参数（通过上下文获取请求，拿到request对象，但不是servlet，获得请求参数getQueryParams()查询参数，k是参数名称，v是值）
		ServerHttpRequest request=exchange.getRequest();
		MultiValueMap<String, String> params = request.getQueryParams();
		//2.获取参数中的authorization参数（取出第一个匹配的authorization）
		String auth=params.getFirst("authorization");
		//3.判断参数值是否等于admin
		if("admin".equals(auth)){
			//4.是，放行（调用下一个过滤器的filter，相当于放行了）
			return chain.filter(exchange);
		}
		//5.否，拦截
		//5.1设置状态码
		exchange.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED);
		//5.2拦截请求（通过exchange得到Response，然后直接结束）
		return exchange.getResponse().setComplete();
	}

	@Override
	public int getOrder() {
		return -1;
	}
	/**
	 * getOrder()方法的效果跟@Order()效果一样
	 */

}
