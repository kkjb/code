package cn.itcast.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * 网关的作用：
 * 1.对用户请求做身份认证、权限校验
 * 2.将用户请求路由到微服务，并实现负载均衡
 * 3.对用户请求做限流
 */

/**
 * 搭建网关服务：
 * 1.创建新的module，引入SpringCloudGateway依赖和nacos的服务发现依赖
 * 2.编写路由配置和nacos地址
 */
@SpringBootApplication
public class GatewayApplication {
	public static void main(String[] args) {
		SpringApplication.run(GatewayApplication.class, args);
	}

}
