package cn.itcast.user.web;

import cn.itcast.user.config.PatternProperties;
import cn.itcast.user.pojo.User;
import cn.itcast.user.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * nacos配置更改后，微服务可以实现热更新，方式：
 * 1.通过@Value注解注入，结合@RefreshScope
 * 2.通过PatternProperties.java的@ConfigurationProperties注入，自动刷新
 * 注意事项：
 * 1.不是所有的配置都适合放到配置中心，维护起来比较麻烦
 * 2.建议将一些关键参数，需要运行时调整的参数放到nacos配置中心，一般都是自定义配置
 */

/**
 * 微服务会从nacos读取的配置文件：
 * 1.[服务名]-[spring.profile.active].yaml，环境配置
 * 2.[服务名].yaml，默认配置，多环境共享
 */
//多种配置文件的优先级：[服务名]—[环境].yaml>[服务名].yaml>本地配置
@Slf4j
@RestController
@RequestMapping("/user")
// @RefreshScope
/**
 * @RefreshScope
 * 配置的自动更新，不用重启微服务
 */
public class UserController {
    @Autowired
    private UserService userService;
    // 注入nacos中的配置属性
    // @Value("${pattern.dateformat}")
    // private String dateFormat;
    // 编写controller，通过日期格式化器来格式化现在的时间并返回
    @Autowired
    private PatternProperties properties;// 属性注入的一种方式，把他注册到一个对象里面去，然后再去用
    @GetMapping("prop")
    public PatternProperties properties(){
        return properties;
    }

    @GetMapping("now")
    public String now() {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern(properties.getDateformat()));
    }

    /**
     * 路径： /user/110
     *
     * @param id 用户id
     * @return 用户
     */
    @GetMapping("/{id}")
    public User queryById(@PathVariable("id") Long id,
                          @RequestHeader(value = "Truth",required = false) String truth) {
        System.out.println("truth:"+truth);
        return userService.queryById(id);
    }
}
