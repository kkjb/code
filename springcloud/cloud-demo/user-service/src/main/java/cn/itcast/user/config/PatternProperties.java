package cn.itcast.user.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
//配置的自动更新，不用重启微服务
@Data
@Component
/**
 * @Component注册成spring的一个bean
 */
@ConfigurationProperties(prefix = "pattern")
//(prefix = "pattern")和dateformat两者的拼接要和配置一致
public class PatternProperties {
    private String dateformat;
    private String envSharedValue;
    private String name;
}
