package cn.itcast.order;

import cn.itcast.feign.clients.UserClient;
import cn.itcast.feign.config.DefaultFeignConfiguration;
import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * EurekaServer：服务端，注册中心
 * 记录服务信息
 * 心跳监控
 * EurekaClient：客户端
 * Provider：服务提供者，例如案例中的user-service
 * 注册自己的信息到EurekaServer
 * 每隔30秒向EurekaServer发送心跳
 * consumer：服消费者，例如案例中的order-service
 * 根据服务名称从EurekaServer拉取服务列表
 * 基于服务列表做负载均衡，选中一个微服务后发送远程调用
 */
/**
 * eureka的作用：
 * 1.消费者该如何获取服务提供者具体信息？
 * 服务提供者启动时向eureka注册自己的信息
 * eureka保存这些信息
 * 消费者根据服务名称向eureka拉取提供者信息
 * 2.如果有多个服务提供者，消费者该如何解决？
 * 服务消费者利用负载均衡算法，从服务列表中挑选一个
 * 3.消费者如何感知服务提供者健康状态？
 * 服务提供者会每隔30秒向EurekaServer发送心跳请求，报告健康状态
 * eureka会更新记录服务列表信息，心跳不正常会被剔除
 * 消费者就可以拉取到最新的信息
 */
@MapperScan("cn.itcast.order.mapper")
@SpringBootApplication
@EnableFeignClients(clients = UserClient.class,defaultConfiguration = DefaultFeignConfiguration.class)
/**
 * @EnableFeignClients
 * 在order-service的启动类种添加注解开启feign的功能
 */
/**
 * feign的日志配置：
 * 1.方法一是配置文件，feign.client.config.xxx.loggerLevel
 * 1.1如果xxx是default则代表全局
 * 1.2如果xxx是服务名称，例如userservice则代表某服务
 * 2.方法二是java代码配置Logger.Level这个Bean
 * 2.1如果在@EnableFeignClients注解声明则代表全局
 * 2.2如果在@FeignClient注解中声明则代表某服务
 */
/**
 * 当定义的FeignClient不在SpringBootApplication的扫描包范围时，这些FeignClient无法使用。有两种方式解决
 * 方法一：指定FeignClient所在的包（不能精准指定，他下面可能还有其他的客户端）
 * @EnableFeignClients(basePackages = "cn.itcast.feign.clients")
 * 方法二：指定FeignClient字节码（指定我要加载哪个客户端）（精准指定）
 * @EnableFeignClients(clients = {UserClient.class})
 */
public class OrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(OrderApplication.class, args);
    }

    /**
     * 创建RestTemplate并注入Spring容器
     */
    /**
     *1.注册RestTemplate
     *在order-service的OrderApplication中注册RestTemplate
     */
    //要想实现跨服务远程调用，就是发送一次http请求，向spring容器里注入一个RestTemplate对象
    @Bean
    //负载均衡的注解
    @LoadBalanced
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }
    // //order服务访问所有微服务都会执行的调用方案
    // @Bean
    // public IRule randomRule(){
    //     return new RandomRule();
    // }

}