package cn.itcast.order.service;

import cn.itcast.feign.clients.UserClient;
import cn.itcast.feign.pojo.User;
import cn.itcast.order.mapper.OrderMapper;
import cn.itcast.order.pojo.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 2.服务远程调用RestTemplate
 * 修改order-service中的OrderService的queryOrderById方法
 */
//利用RestTemplate的方法getForObject告诉请求的url路径（就像浏览器一样），然后告诉他返回值的类型，会帮你把返回的json转成对应的类型，就能去使用了
@Service
public class OrderService {

	@Autowired
	private OrderMapper orderMapper;

	//用feign客户端代替RestTemplate
	@Autowired
	private UserClient userClient;

	public Order queryOrderById(Long orderId) {
		// 1.查询订单
		Order order = orderMapper.findById(orderId);
		// 2.用Feign远程调用
		User user = userClient.findById(order.getUserId());
		// 3.封装user信息到Order
		order.setUser(user);
		// 4.返回
		return order;
/*     @Autowired
    private RestTemplate restTemplate;

    public Order queryOrderById(Long orderId) {
        // 1.查询订单
        Order order = orderMapper.findById(orderId);
        // 2.利用RestTemplate发起http请求，查询用户
        // 2.1.url路径
        String url = "http://userservice/user/" + order.getUserId();
        // 2.2.发送http请求，实现远程调用
        User user=restTemplate.getForObject(url,User.class);
        //3.封装user信息到Order
        order.setUser(user);
        // 4.返回
        return order;
        */
	}

}
