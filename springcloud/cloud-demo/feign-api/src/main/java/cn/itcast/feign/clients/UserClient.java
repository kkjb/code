package cn.itcast.feign.clients;

import cn.itcast.feign.pojo.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

//编写feign的客户端

/**
 * 服务名称：userservice
 * 请求方式：GET
 * 请求路径：/user/{id}
 * 请求参数：Long id
 * 返回值类型：User
 */
@FeignClient("userservice")
public interface UserClient {

	@GetMapping("/user/{id}")
	User findById(@PathVariable("id") Long id);
}
/**
 *feign的最佳实践
 * 方法1.继承给消费者的FeignClient和提供者的controller定义统一的父接口作为标准
 * 缺点：1.服务紧耦合2.父接口参数列表中的映射不会被继承
 * 写法：
 * public interface UserAPI {
 *     @GetMapping("/user/{id}")
 *        User findById(@PathVariable("id") Long id);
 * }
 * @FeignClient("userservice")
 * public interface UserClient extends UserApi{
 * }
 * @RestController
 * public class UserController implements UserAPI{
 * 	public User findById(@PathVariable("id") Long id){
 * 		//...实现业务
 *        }
 * }
 *方法2（抽取）：将FeignClient抽取为独立模块，并将接口有关的POJO、默认Feign配置都放到这个模块中，提供给所有消费者使用
 * 步骤：1.首先创建一个module，命名为feign-api，然后引入feign的starter依赖
 * 2.将order-service中编写的UserClient、User、DefaultFeignConfiguration都复制到feign-api项目中
 * 3.在order-service中引入feign-api的依赖
 * 4.修改order-service中的所有与上述三个组件有关的import部分，改成导入feign-api中的包
 * 5.重启测试
 */